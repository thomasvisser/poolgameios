//
//  LocalizationHelper.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 06-08-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import Foundation

/**
Localization Helper class, which will act as a bridge between the NSLocalization and the
 implementation of it. This class provides functions which make it possible to add text
 dynamically during runtime, but still provide localization features.
*/
public class LocalizationHelper {
    
    private static let instance = LocalizationHelper()
    
    public static func getInstance() -> LocalizationHelper{
        return instance
    }
    
    private init(){}
    
    /**
    Gets the right localized Game Over message, based on the given key. It will also dynamically
    add winner and loser to the localized String
     - parameters:
        - key: The key String to identify the correct localization text.
        - winner: The player which won the game
        - loser: The player which lost the game
    */
    public func localizeGameOverMessage(key: String, winner: String, loser: String) -> String {
        
        var localizedMessage = key.localized
        
        //Replace win placeholder with winner
        localizedMessage = localizedMessage.replacingOccurrences(of: "%win", with: winner)
        
        //Replace lose placeholder with loser
        localizedMessage = localizedMessage.replacingOccurrences(of: "%lose", with: loser)
        
        return localizedMessage
    }
    
    /**
    Gets the right localized Ball Potted message. It will dynamically add
    the player name and the potted ball name to the localized String
     - parameters:
         - playerName: the player's name
         - ball: the potted ball name
    */
    public func localizeBallPottedMessage(playerName: String, ball: String) -> String{
        
        var localizedMessage = "playerPotted".localized
        
        //Replace ball placeholder with ball number
        localizedMessage = localizedMessage.replacingOccurrences(of: "%ball", with: ball)
        
        //Replace name placeholder with player name
        localizedMessage = localizedMessage.replacingOccurrences(of: "%player", with: playerName)
        
        return localizedMessage
    }
    
    /**
    Gets the right localized Ball Potted message. It will dynamically add
    the player name to the localized String
     - parameters:
        - playerName: the player's name
     */
    public func localizePlayerTurnMessage(playerName: String) -> String {
        
        var localizedMessage = "playerTurn".localized
        
        //Replace name placeholder with player name
        localizedMessage = localizedMessage.replacingOccurrences(of: "%player", with: playerName)
        
        return localizedMessage
    }
}
