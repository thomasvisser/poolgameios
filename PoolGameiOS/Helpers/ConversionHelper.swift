//
//  ConversionHelper.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 16-07-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import Foundation

class ConversionHelper {
    
    private static let instance = ConversionHelper()
    
    public static func getInstance() -> ConversionHelper {
        return instance
    }
    
    /**
    Converts the custom velocity scale value, to the real scene velocity
     - parameters:
        - velocity: the custom velocity scale to convert
    */
    public func convertVelocity(velocity: Float) -> Float{
        
        //Minimum and Maximum custom range values of velocity
        let customMinRange: Float = 0.04
        let customMaxRange: Float = 0.3
        
        //The amount of different levels to choose from
        //within the custom range
        let rangeSteps: Float = Global.velocitySteps
        
        let validatedVelocity = Validator.getInstance().validateVelocity(velocity: velocity, max: rangeSteps)
    
        //Difference between Maxixum and Minimum range values
        let diffRange: Float = customMaxRange - customMinRange
        
        let finalVelocity: Float = customMinRange + (validatedVelocity * (diffRange/rangeSteps))
        
        return finalVelocity
    }
    
    /**
    Converts a radian number to degrees
    - parameters:
        - rad: the amount of radians to convert to degrees
    */
    public func convertRadToDegree(rad: Float) -> Float {
        return (rad * 180)/Float.pi
    }
    
    /**
    Convert a degree number to radius
     - parameters:
        - deg: the amount of degrees to convert to radians
    */
    public func convertDegreeToRad(deg: Float) -> Float {
        return deg * Float.pi / 180
    }
    
    /**
    Converts the Ball Number type to a corresponding Ball Type
     - parameters:
        - number: the ball number to convert
    */
    public func convertBallNumbToBallType(number: BallNumber) -> BallType{
     
        if number.rawValue <= 8 {
            return BallType.FullBall
        }else{
            return BallType.HalfBall
        }
    }
    
    /**
    Converts the given String holeName to a CollisionHoleType Enum Object
     - parameters:
        - holeName: the String representation of the Hole to convert to a CollisionType
    */
    public func convertToHoleType(holeName: String) -> CollisionHoleType?{
        
        if holeName.contains(find: "TopLHole"){
            return CollisionHoleType.Top_Left_Hole
        }else if holeName.contains(find: "TopRHole"){
            return CollisionHoleType.Top_Right_Hole
        }else if holeName.contains(find: "LeftMHole"){
            return CollisionHoleType.Left_Middle_Hole
        }else if holeName.contains(find: "RightMHole"){
            return CollisionHoleType.Right_Middle_Hole
        }else if holeName.contains(find: "BottomLHole"){
            return CollisionHoleType.Bottom_Left_Hole
        }else if holeName.contains(find: "BottomRHole"){
            return CollisionHoleType.Bottom_Right_Hole
        }
        
        return nil
    }
    
    /**
    Returns the oppisite siding hole of the given hole. This will be used as
     the 8ball destination hole.
     - parameters:
        - hole: the current hole type to get the opposite hole for
    */
    public func getOppositeHole(hole: CollisionHoleType) -> CollisionHoleType {
        
        switch hole {
            case .Top_Left_Hole:
                return .Bottom_Right_Hole
            case .Top_Right_Hole:
                return .Bottom_Left_Hole
            case .Left_Middle_Hole:
                return .Right_Middle_Hole
            case .Right_Middle_Hole:
                return .Left_Middle_Hole
            case .Bottom_Left_Hole:
                return.Top_Right_Hole
            case .Bottom_Right_Hole:
                return .Top_Left_Hole
        }
    }
}
