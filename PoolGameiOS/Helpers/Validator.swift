//
//  Validator.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 16-07-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import Foundation

class Validator {
    
    private static let instance = Validator()
    
    public static func getInstance() -> Validator{
        return instance
    }
    
    private init(){}
    
    /**
    Validates if the entered velocity is in range
     - parameters:
         - velocity: the velocity value to check the validity for
         - max: the maximum allowed velocity value
    */
    public func validateVelocity(velocity: Float, max: Float) -> Float{
        
        var newVelocity: Float = velocity
        if velocity < 0 {
            newVelocity = 0
        }else if velocity > max {
            newVelocity = max
        }
        
        return newVelocity
    }
}
