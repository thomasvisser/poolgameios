//
//  GameViewController.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 03-07-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import UIKit
import QuartzCore
import SceneKit

class GameViewController: UIViewController {
    
    //Scene and View Nodes
    var sceneView: CustomSCNView!
    var mainScene: SCNScene!
    
    //The HUD and GameMenu of the Game
    var overlayScene: OverlayScene!
    var gameMenu: MenuScreen!
    
    //Camera Nodes
    var topCamera: SCNNode!
    
    //The table property, which contains all details for the table
    var table: Table!
    var updateLoopTimer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setup game
        self.setupView()
        self.setupScene()
        self.setupNodes()
        self.setupBalls()
        self.setupTable()
        
        //Set Delegate connections
        CollisionDetection.delegate = self
        Game.getInstance().delegate = self
                
        //Setup Timer to initiate the update loop
        //The timer will update for 30 fps (0.033333 s) or 60 fps (0.016666)
        self.updateLoopTimer = Timer.scheduledTimer(timeInterval: 0.016666, target: self, selector: #selector(updateRenderer(_:)), userInfo: nil, repeats: true)
    }
    
    public func setupView(){

        self.sceneView = CustomSCNView(frame: self.view.frame)
        
        self.overlayScene = OverlayScene(size: CGSize(width:self.sceneView.bounds.width, height: self.sceneView.bounds.height))
        self.overlayScene.overlayDelegate = self
        self.sceneView.overlaySKScene = self.overlayScene.hudScene
        
        self.view.addSubview(self.sceneView)
    }
    
    public func setupScene(){
        
        //Loads the scene from the assets
        self.mainScene = SCNScene(named: "art.scnassets/pool-table.scn")!
        
        //Point the Contact Delegate Handling to self
        self.mainScene.physicsWorld.contactDelegate = self

        self.mainScene.rootNode.addChildNode(self.sceneView.follow_Camera)
        
        //Point the scene of the view to the mainScene
        self.sceneView.scene = self.mainScene
        self.sceneView.pointOfView = self.sceneView.customCamera
    }
    
    public func setupNodes(){
        
        //Loads the top Camera from the Scene
        self.topCamera = self.mainScene.rootNode.childNode(withName: "camera-top", recursively: true)
    }
    
    /**
    Loads the Ball Nodes from the Scene and attaches it the the Model.
    It pushes every ball to the internal Balls Array to be used in the code
    */
    public func setupBalls(){
        
        //Adds balls
        // Full Balls
        let oneBall = self.mainScene.rootNode.childNode(withName: "oneBall", recursively: true)!
        let twoBall = self.mainScene.rootNode.childNode(withName: "twoBall" , recursively: true)!
        let threeBall = self.mainScene.rootNode.childNode(withName: "threeBall", recursively: true)!
        let fourBall = self.mainScene.rootNode.childNode(withName: "fourBall", recursively: true)!
        let fiveBall = self.mainScene.rootNode.childNode(withName: "fiveBall", recursively: true)!
        let sixBall = self.mainScene.rootNode.childNode(withName: "sixBall", recursively: true)!
        let sevenBall = self.mainScene.rootNode.childNode(withName: "sevenBall", recursively: true)!
        
        // Half Balls
        let eightBall = self.mainScene.rootNode.childNode(withName: "eightBall", recursively: true)!
        let nineBall = self.mainScene.rootNode.childNode(withName: "nineBall", recursively: true)!
        let tenBall = self.mainScene.rootNode.childNode(withName: "tenBall", recursively: true)!
        let elevenBall = self.mainScene.rootNode.childNode(withName: "elevenBall", recursively: true)!
        let twelveBall = self.mainScene.rootNode.childNode(withName: "twelveBall", recursively: true)!
        let thirteenBall = self.mainScene.rootNode.childNode(withName: "thirteenBall", recursively: true)!
        let fourteenBall = self.mainScene.rootNode.childNode(withName: "fourteenBall", recursively: true)!
        let fifteenBall = self.mainScene.rootNode.childNode(withName: "fifteenBall", recursively: true)!
        
        // White Ball
        let whiteBall = self.mainScene.rootNode.childNode(withName: "whiteBall", recursively: true)!

        //Add Balls to Model
        Balls.getInstance().addBall(ball: Ball(name: "oneBall", number: .OneBall, node: oneBall))
        Balls.getInstance().addBall(ball: Ball(name: "twoBall", number: .TwoBall, node: twoBall))
        Balls.getInstance().addBall(ball: Ball(name: "threeBall", number: .ThreeBall, node: threeBall))
        Balls.getInstance().addBall(ball: Ball(name: "fourBall", number: .FourBall, node: fourBall))
        Balls.getInstance().addBall(ball: Ball(name: "fiveBall", number: .FiveBall, node: fiveBall))
        Balls.getInstance().addBall(ball: Ball(name: "sixBall", number: .SixBall, node: sixBall))
        Balls.getInstance().addBall(ball: Ball(name: "sevenBall", number: .SevenBall, node: sevenBall))
        Balls.getInstance().addBall(ball: Ball(name: "eightBall", number: .EightBall, node: eightBall))
        Balls.getInstance().addBall(ball: Ball(name: "nineBall", number: .NineBall, node: nineBall))
        Balls.getInstance().addBall(ball: Ball(name: "tenBall", number: .TenBall, node: tenBall))
        Balls.getInstance().addBall(ball: Ball(name: "elevenBall", number: .ElevenBall, node: elevenBall))
        Balls.getInstance().addBall(ball: Ball(name: "twelveBall", number: .TwelveBall, node: twelveBall))
        Balls.getInstance().addBall(ball: Ball(name: "thirteenBall", number: .ThirteenBall, node: thirteenBall))
        Balls.getInstance().addBall(ball: Ball(name: "fourteenBall", number: .FourteenBall, node: fourteenBall))
        Balls.getInstance().addBall(ball: Ball(name: "fifteenBall", number: .FifteenBall, node: fifteenBall))
        Balls.getInstance().addBall(ball: Ball(name: "whiteBall", number: .WhiteBall, node: whiteBall))
        
        //Place all the balls in the Original Game Position
        Balls.getInstance().resetBallPositions()
        
        //Set correct position for the Camera
        self.updateCameraPosition()
        
        //TESTING
        //Give whiteBall an initial direction and velocity
        //Balls.getInstance().setBallDirection(direction: SCNVector3(x: -1, y: 0, z: -1), withName: "whiteBall")
        //Balls.getInstance().setBallVelocity(velocity: 9, withName: "whiteBall")
    }
    
    /**
    Loads the exisiting Collision Walls from the main Scene, and attaches this information
    to the Table Object.
    */
    public func setupTable(){
    
        //let table = self.mainScene.rootNode.childNode(withName: "PoolTable", recursively: true)!
        //let tableWidth = table.boundingBox.max.x - table.boundingBox.min.x
        //let tableHeight = table.boundingBox.max.z - table.boundingBox.min.z
        
        //Note: HARDCODED POSITIONS!!
        let tableWidth = Float(8.94)
        let tableHeight = Float(15.086)
        
        //Initialize Table property to fill data
        self.table = Table(scene: self.mainScene, width: tableWidth, height: tableHeight)
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
}

/**
Implementation methods of the TableRequests Delegate (as described in CollisionDetection)
Methods are implemented here to get information from the locally stored Table Object.
*/
extension GameViewController: TableRequestsDelegate {

    func requestTableDimensions() -> (width: Float, height: Float) {
        return self.table.getDimensions()
    }
    
    func requestTableCollisionWall(cType: CollisionWallType) -> SCNNode {
        return self.table.getCollisionWall(type: cType)
    }
    
    func requestMidHoles() -> Array<SCNNode> {
        return self.table.getMidHoles()
    }
    
    func requestCornerHoles() -> Array<SCNNode> {
        return self.table.getCornerHoles()
    }
}

/**
Implementation methods of the hudInteraction Delegate (as described in OverlayScene)
Methods are implemeted here to change the point of view of the sceneView's camera.
*/
extension GameViewController: HudInteractionDelegate {
    
    func switchPointOfView(state: String) {
        
        if state == "2D"{
            
            //Animate the changing of the point of view
            SCNTransaction.begin()
            SCNTransaction.animationDuration = 1.0
            self.sceneView.pointOfView = self.topCamera
            SCNTransaction.commit()
        }else if state == "3D"{
            
            //Animate the changing of the point of view
            SCNTransaction.begin()
            SCNTransaction.animationDuration = 1.0
            self.sceneView.pointOfView = self.sceneView.customCamera
            SCNTransaction.commit()
        }
    }
}

extension GameViewController: GameMenuDelegate {
    
    /**
    Displays the GameMenu to the screen, as part of the HUD.
     - parameters:
        - withMessage: the message to display in the GameMenu info section
    */
    func showGameMenuScreen(withMessage: String) {
        
        self.gameMenu = MenuScreen(parentNode: self.overlayScene.hudScene, infoLabelText: withMessage)
        self.gameMenu.delegate = self
        
        self.overlayScene.hudScene.addChild(gameMenu)
        
        //Sent a notification to inform that gameMenu will be shown (part of the HUD interaction)
        NotificationCenter.default.post(name: Global.HUDNotificName, object: nil, userInfo: ["hudInteraction": true])
        self.overlayScene.disableHudElements()
    }
    
    /**
    Removes the GameMenu from its parent (the HUD), because the user interacted with one of the
    GameMenu buttons, which means the GameMenu screen can be dismissed.
    */
    func removeGameMenuScreen() {
        
        self.gameMenu.removeFromParent()
        
        //Sent a notification to inform that gameMenu will be shown (part of the HUD interaction)
        NotificationCenter.default.post(name: Global.HUDNotificName, object: nil, userInfo: ["hudInteraction": false])
        self.overlayScene.enableHudElements()
    }
    
    func goToSplashScreen(){
        
        //Sent a notification to the Splas Screen View Controller, to clear the usernames,
        // before dismissing the Game Scene View
        NotificationCenter.default.post(name: Global.SplashScreenNotificName, object: nil, userInfo: ["clearFields": true])
        
        self.dismiss(animated: true, completion: nil)
    }
}


