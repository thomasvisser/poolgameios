//
//  BallType.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 17-07-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import Foundation

/**
Defines the 2 types a Ball can be (Full or Half)
*/
public enum BallType: UInt32 {
    case FullBall
    case HalfBall
}
