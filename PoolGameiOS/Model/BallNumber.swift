//
//  BallType.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 04-07-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import Foundation

/**
Defines all the different numbers a Ball Object can be.
*/
public enum BallNumber: Int {
    case WhiteBall = 0
    case OneBall = 1
    case TwoBall = 2
    case ThreeBall = 3
    case FourBall = 4
    case FiveBall = 5
    case SixBall = 6
    case SevenBall = 7
    case EightBall = 8
    case NineBall = 9
    case TenBall = 10
    case ElevenBall = 11
    case TwelveBall = 12
    case ThirteenBall = 13
    case FourteenBall = 14
    case FifteenBall = 15
}
