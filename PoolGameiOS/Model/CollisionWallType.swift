//
//  CollisionWallType.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 15-07-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import Foundation

/**
Defines all the different Collision Wall types of the Pool Table
*/
public enum CollisionWallType {    
    case Wall_Top
    case Wall_Bottom
    case Wall_Left_Top
    case Wall_Left_Bottom
    case Wall_Right_Top
    case Wall_Right_Bottom
    case Floor
}
