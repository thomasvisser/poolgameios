//
//  Table.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 15-07-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import Foundation
import SceneKit

class Table {
    
    var debugTable = false
    
    //Nodes which refer to Scene Collision Wall Objects
    private var Cwall_top: SCNNode!
    private var Cwall_bottom: SCNNode!
    private var Cwall_left_top: SCNNode!
    private var Cwall_left_bottom: SCNNode!
    private var Cwall_right_top: SCNNode!
    private var Cwall_right_bottom: SCNNode!
    private var Cfloor: SCNNode!
    
    //Arrays with Nodes which refer to the Scene Holes
    private var mid_holes: Array<SCNNode> = Array()
    private var corner_holes: Array<SCNNode> = Array()
    
    private var width: Float!
    private var height: Float!
    
    //Create a reference to the main Scene stored in GameViewController
    private var mainSceneRef: SCNScene
    
    init(scene: SCNScene, width: Float, height: Float) {
        
        self.mainSceneRef = scene
        
        self.loadCollisionWalls()
        self.loadHoles()
        self.setupCViewProperties()
        
        self.width = width
        self.height = height
    }
    
    /**
    Loads all the Collision Walls from the Main Scene and adds a reference to them
    in the Table Object
    */
    private func loadCollisionWalls(){
        
        //Add reference to scene Collision Objects
        self.Cwall_top = self.mainSceneRef.rootNode.childNode(withName: "Coll_wall_top" , recursively: true)!
        self.Cwall_bottom = self.mainSceneRef.rootNode.childNode(withName: "Coll_wall_bottom" , recursively: true)!
        self.Cwall_left_top = self.mainSceneRef.rootNode.childNode(withName: "Coll_wall_left_top" , recursively: true)!
        self.Cwall_left_bottom = self.mainSceneRef.rootNode.childNode(withName: "Coll_wall_left_bottom" , recursively: true)!
        self.Cwall_right_top = self.mainSceneRef.rootNode.childNode(withName: "Coll_wall_right_top" , recursively: true)!
        self.Cwall_right_bottom = self.mainSceneRef.rootNode.childNode(withName: "Coll_wall_right_bottom" , recursively: true)!
        self.Cfloor = self.mainSceneRef.rootNode.childNode(withName: "Coll_floor" , recursively: true)!
    }
    
    /**
    Loads all the Collision Holes from the Main Scene and adds a reference to them
    in the Table Object
    */
    private func loadHoles(){
        
        //Add reference to the scene Holes
        self.mid_holes.append(self.mainSceneRef.rootNode.childNode(withName: "LeftMHole", recursively: true)!)
        self.mid_holes.append(self.mainSceneRef.rootNode.childNode(withName: "RightMHole", recursively: true)!)
        
        self.corner_holes.append(self.mainSceneRef.rootNode.childNode(withName: "TopLHole", recursively: true)!)
        self.corner_holes.append(self.mainSceneRef.rootNode.childNode(withName: "TopRHole", recursively: true)!)
        self.corner_holes.append(self.mainSceneRef.rootNode.childNode(withName: "BottomLHole", recursively: true)!)
        self.corner_holes.append(self.mainSceneRef.rootNode.childNode(withName: "BottomRHole", recursively: true)!)

    }
    
    /**
    Sets some properties for the Collision Wall and Collision Holes Reference Objects
    */
    private func setupCViewProperties(){
        
        //Hide all the Collision Objects
        if !debugTable {
            self.Cwall_top.isHidden = true
            self.Cwall_bottom.isHidden = true
            self.Cwall_left_top.isHidden = true
            self.Cwall_left_bottom.isHidden = true
            self.Cwall_right_top.isHidden = true
            self.Cwall_right_bottom.isHidden = true
            self.Cfloor.isHidden = true
            
            for mHoles in self.mid_holes {
                for wall in mHoles.childNodes {
                    wall.geometry?.firstMaterial?.diffuse.contents = UIColor.clear
                }
            }
            
            for cHoles in self.corner_holes {
                for wall in cHoles.childNodes {
                    wall.geometry?.firstMaterial?.diffuse.contents = UIColor.clear
                }
            }
        }
    }
    
    /**
    Returns the width and height of the PoolTable
    */
    public func getDimensions() -> (width: Float, height: Float){
        return (self.width, self.height)
    }
    
    /**
    Returns a specific Table Collision Wall SCNNode
    - parameters:
        -type: the type of Collision Wall to return
    */
    public func getCollisionWall(type: CollisionWallType) -> SCNNode{
        
        switch type {
        case .Wall_Top:
            return self.Cwall_top!
        case .Wall_Left_Top:
            return self.Cwall_left_top!
        case .Wall_Right_Top:
            return self.Cwall_right_top!
        case .Wall_Bottom:
            return self.Cwall_bottom!
        case .Wall_Left_Bottom:
            return self.Cwall_left_bottom!
        case .Wall_Right_Bottom:
            return self.Cwall_right_bottom!
        case .Floor:
            return self.Cfloor!
        }
    }
    
    /**
    Returns the 2 Mid Hole Reference Objects present in the mid_holes Array
    */
    public func getMidHoles() -> Array<SCNNode>{
        return self.mid_holes
    }
    
    /**
    Returns the 4 Corner Hole Reference Objects present in the corner_holes Array
    */
    public func getCornerHoles() -> Array<SCNNode>{
        return self.corner_holes
    }
}
