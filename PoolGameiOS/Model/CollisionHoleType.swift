//
//  CollisionHoleType.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 07-08-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import Foundation

/*
 Defines all the different Hole types of the Pool Table
*/
public enum CollisionHoleType {
    case Top_Left_Hole
    case Top_Right_Hole
    case Left_Middle_Hole
    case Right_Middle_Hole
    case Bottom_Left_Hole
    case Bottom_Right_Hole
}
