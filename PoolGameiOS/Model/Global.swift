//
//  Global.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 30-07-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import Foundation

//Acts as a Global Data Library which contains data that needs
// to be accessable troughout the whole Codebase
public class Global {
    
    static let velocitySteps: Float = 10
    
    //Observer Notification Names
    static let HUDNotificName = Notification.Name("HUDInteracted")
    static let SplashScreenNotificName = Notification.Name("MoveToSplash")
    static let LabelNotificName = Notification.Name("LabelUpdate")
    static let RoundPlayedName = Notification.Name("RoundPlayedName")
}
