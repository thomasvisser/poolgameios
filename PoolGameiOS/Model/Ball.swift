//
//  Ball.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 04-07-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import Foundation
import SceneKit

class Ball {
    
    private var name: String!
    private var ballNode: SCNNode!
    private var ballGeometry: SCNGeometry!
    
    private var number: BallNumber!
    private var type: BallType!
    
    private var ballPotted = false
    private var pottedHole: CollisionHoleType!
    
    private var position: SCNVector3!
    private var lastPosition: SCNVector3!
    
    private let radius: CGFloat = 0.18
    private var direction: SCNVector3!
    private var velocity: Float!
    
    private var friction: Float = 0
    private let gravityForce: Float = 1
    
    public func getName() -> String {
        return self.name
    }
    
    public func getPosition() -> SCNVector3{
        return self.position
    }
    
    public func getLastPosition() -> SCNVector3{
        return self.lastPosition
    }
    
    public func setPosition(x: Float, y: Float, z: Float){
        self.position = SCNVector3(x: x, y: y, z: z)
    }
    
    public func setPosition(position: SCNVector3){
        self.position = position
    }
    
    public func setLastPosition(lastPos: SCNVector3){
        self.lastPosition = lastPos
    }
    
    public func getRadius() -> Float {
        return Float(self.radius)
    }
    
    public func getNumber() -> Int {
        return self.number.rawValue
    }
    
    public func getType() -> BallType {
        return self.type
    }
    
    public func getVelocity() -> Float {
        return self.velocity
    }
    
    public func isPotted() -> Bool {
        return self.ballPotted
    }
    
    public func getPottedHole() -> CollisionHoleType {
        return self.pottedHole
    }
    
    public func getDirection() -> SCNVector3 {
        return self.direction
    }
    
    public func setVelocity(newVelocity: Float){
        self.velocity = newVelocity
    }
    
    public func setBallPotted(isPotted: Bool){
        self.ballPotted = isPotted
    }
    
    public func setPottedHole(newHole: CollisionHoleType?){
        self.pottedHole = newHole
    }
    
    public func setDirection(newDirection: SCNVector3){
        self.direction = newDirection
    }
    
    init(name: String, number: BallNumber, position: (x: Float, y: Float, z: Float)){
        
        let tempGeo: SCNGeometry = SCNSphere(radius: self.radius)
        self.ballNode = SCNNode(geometry: tempGeo)
        self.ballGeometry = self.ballNode.geometry!
        self.name = name
    
        self.setPosition(x: position.x, y: position.y, z: position.z)
        
        self.velocity = 0
        self.direction = SCNVector3(x: 0, y: 0, z: 0)
        
        self.number = number
        self.type = ConversionHelper.getInstance().convertBallNumbToBallType(number: number)
    }
    
    init(name: String, number: BallNumber, node: SCNNode){
        
        self.ballNode = node
        self.ballGeometry = node.geometry!
        self.name = name
        
        self.setPosition(x: node.position.x, y: node.position.y, z: node.position.z)
        
        self.velocity = 0
        self.direction = SCNVector3(x: 0, y: 0, z: 0)
        
        self.number = number
        self.type = ConversionHelper.getInstance().convertBallNumbToBallType(number: number)
    }
    
    /**
    Calculates and updates the values of the Ball Node Object,
    based on the velocity and direction of the Ball
    */
    public func updateNodePositions(){
        
        self.ballNode.position.x = self.getPosition().x
        self.ballNode.position.y = self.getPosition().y
        self.ballNode.position.z = self.getPosition().z
        
        if let velo = self.velocity {
            if velo > 0{
            
                guard let dir = self.direction else { print("No Direction set for Ball! \(self.name)"); return }
                
                //Determines the increment values for the x- and z-axis.
                //These values will be added to the current position
                let incrementX: Float = dir.x * velo
                let incrementY: Float = dir.y * velo
                let incrementZ: Float = dir.z * velo
    
                let newPosX = self.getPosition().x + incrementX
                let newPosY = self.getPosition().y + incrementY
                let newPosZ = self.getPosition().z + incrementZ
                
                //Sets the new position for the Ball Object
                self.setPosition(x: newPosX, y: newPosY, z: newPosZ)
            
                //The lowest velocity value a ball can have.
                //If the value goes lower, the ball has practically a velocity of 0
                let minimumVelocity: Float = 0.01
                
                //Reduces the speed (velocity) of the Ball, due to the x amount of friction
                //that works on the Ball
                if self.velocity > minimumVelocity {
                    self.velocity = self.velocity - self.friction
                    self.calculateBallFriction()
                }else{
                    self.velocity = 0
                }
            }
        }
    }
    
    /**
    Calculates the friction the Ball will have,
    based on the velocity of the Ball
    */
    private func calculateBallFriction(){
        self.friction = self.velocity * 0.012
    }
}
