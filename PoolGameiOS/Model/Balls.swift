//
//  Balls.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 04-07-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import Foundation
import SceneKit

public class Balls {
    
    private static let instance: Balls = Balls()
    
    public static func getInstance() -> Balls {
        return instance
    }
    
    private init(){}
    
    private var balls: Array<Ball> = Array()
    
    //MARK: Core functions
    //=====================
    /**
    Adds a Ball object to the internal balls Array.
    - parameters:
        - ball: The Ball object to add to the internal balls Array
    */
    func addBall(ball: Ball){
        self.balls.append(ball)
    }
    
    /**
    Removes a Ball object from the internal balls Array.
    - parameters:
        - ball: The Ball object to remove from the internal balls Array
    */
    func removeBall(ballToR: Ball){
     
        var index: Int = 0
    
        //Check if ball exists in the balls Array. If yes, remove the ball
        for ball in self.balls {
            if ball.getName() == ballToR.getName() {
                self.balls.remove(at: index)
                return
            }
            index = index + 1
        }
    }
    
    /**
    Return the amount of Ball Objects in the internal balls Array.
    */
    func getAmountOfBalls() -> Int {
        return self.balls.count
    }
    
    /**
    Returns the position of a specific Ball
    - parameters:
        - withName: The name of the Ball Object to get the position of
    */
    func getBallPosition(withName: String) -> SCNVector3?{
        
        for ball in self.balls {
            
            if ball.getName() == withName {
                return ball.getPosition()
            }
        }
        
        return nil
    }
    
    /**
    Returns the radius of a specific Ball
    - parameters:
        - withName: The name of the Ball Object to get the radius of
    */
    func getBallRadius(withName: String) -> Float? {
        
        for ball in self.balls {
            
            if ball.getName() == withName {
                return ball.getRadius()
            }
        }
        
        return nil
    }
    
    /**
     Returns the last position of a specific Ball
     - parameters:
        - withName: The name of the Ball Object to get the last position of
     */
    func getLatestBallPos(withName: String) -> SCNVector3? {
        
        for ball in self.balls {
            
            if ball.getName() == withName {
                return ball.getLastPosition()
            }
        }
        
        return nil
    }
}

// MARK: SET BALL ELEMENTS
//==============================
//Extension of the Balls Class, which contains additional functions
// for accessing Ball Elements
extension Balls {

    /**
     Sets the direction of the Ball Object.
     - parameters:
     - direction: The new direction to set for the Ball Object
     - withName: The name of the Ball Object to change the direction of
     */
    func setBallDirection(direction: SCNVector3, withName: String){
        
        for ball in self.balls {
            if ball.getName() == withName {
                ball.setDirection(newDirection: direction)
            }
        }
    }
    
    /**
    Sets the velocity of the Ball Object.
    Velocity can be a value between 1..10
     - parameters:
        - velocity: The new velocity to set for the Ball Object
        - withName: The name of the Ball Object to change the velocity of
    */
    func setBallVelocity(velocity: Float, withName: String){
        
        for ball in self.balls {
            
            if ball.getName() == withName {
                let newVelocity = ConversionHelper.getInstance().convertVelocity(velocity: velocity)
                ball.setVelocity(newVelocity: newVelocity)
            }
        }
    }
    
    /**
    Sets the Hole Type, where the ball is potted into.
     - parameters:
        - potHole: The hole the ball is potted into
        - withName: The name of the Ball Object to set the potted hole for
    */
    func setBallPottedHole(potHole: CollisionHoleType, withName: String){
        
        for ball in self.balls {
            
            if ball.getName() == withName {
                ball.setPottedHole(newHole: potHole)
            }
        }
    }
    
    /**
    Sets the last Position of the ball with given name
     - parameters:
        - lastPos: The last position of the ball
        - withName: The name of the ball to store the last position for
    */
    func setLatestBallPos(ofBall: String){
        
        for ball in self.balls {
            
            if ball.getName() == ofBall {
                
                let currentPos = ball.getPosition()
                ball.setLastPosition(lastPos: currentPos)
            }
        }
    }
}


// MARK: COLLISION DETECTION BALLS
//================================
//Extension of the Balls Class, which contains additional functions
// for checking Collision of the Balls
extension Balls {

    /**
     Checks if any of the Ball Objects collides with the Walls of the Pool Table.
     */
    func checkCollisions(){
        
        for ball in self.balls {
            
            //If ball is already potted, no need to check collisions for the ball
            if !ball.isPotted(){
                CollisionDetection.getInstance().checkWallCollision(ball: ball)
                CollisionDetection.getInstance().checkHolesCollision(ball: ball)
            }
        }
    }
    
    /**
     Checks collision for all the balls on the field.
     */
    func checkBallCollision(){
        
        var tempIndex: Int = 0
        for ball in self.balls {
            
            //If ball is already potted, no need to check collisions for the ball
            if !ball.isPotted(){
                for index in (tempIndex+1)..<self.balls.count {
                    CollisionDetection.getInstance().checkBallCollision(ballA: ball, ballB: self.balls[index])
                }
            }
            tempIndex = tempIndex + 1
        }
    }
    
    //NOTE: Maybe not neccessary after all?
    /**
     Checks collision only for the specific ball described by the filter propery.
     - parameters:
     - name: the name of the ball to check collision for.
     */
    func checkBallCollision(withFilter name: String){
        
        var tempIndex: Int = 0
        var tempFilterBall: (ball: Ball, index: Int)!
        
        for ball in self.balls {
            
            if ball.getName() == name {
                tempFilterBall = (ball, tempIndex)
            }
            tempIndex = tempIndex + 1
        }
        
        tempIndex = 0
        
        for index in 0..<self.balls.count {
            
            if index != tempFilterBall.index {
                CollisionDetection.getInstance().checkBallCollision(ballA: tempFilterBall.ball, ballB: self.balls[index])
            }
            tempIndex = tempIndex + 1
        }
    }
}


// MARK: BASIC CHECKER FUNCTIONS
//==============================
//Extension of the Balls Class, which contains additional functions
// for checking certain tasks against the private Ball Array
extension Balls {
    
    /**
     Checks if any of the Balls has any velocity left.
     Returns 'true' if any ball has a velocity, return 'false' if
     no balls have a velocity
     */
    func checkBallVelocity() -> Bool{
        
        var ballsHaveVelocity = false
        
        for ball in self.balls {
            
            if ball.getVelocity() > 0 {
                ballsHaveVelocity = true
                break
            }
        }
        
        return ballsHaveVelocity
    }
    
    func checkBallsPotted() -> Array<Ball>{
        
        var pottedBalls: Array<Ball> = Array()
        
        for ball in self.balls {
            
            if ball.isPotted(){
                pottedBalls.append(ball)
            }
        }
        
        return pottedBalls
    }
}


// MARK: UPDATE BALLS
//===================
//Extension of the Balls Class, which contains additional functions
// to update certain properties of the Ball Objects part of the private Balls Array
extension Balls {

    /**
     Updates all the Balls present in the balls Array. It updates all the
     Ball properties for every frame the render loop renders
     */
    func updateBalls(){
    
        for ball in self.balls {
            
            //If ball is already potted, no need to check collisions for the ball
            if !ball.isPotted() || ball.getName() == "whiteBall"{
                ball.updateNodePositions()
            }
        }
    }
    
    /**
     Resets all Ball positions so that all balls will be in their starting triangle
     and the whiteBall in its seperate starting position
     */
    func resetBallPositions(){
        
        //Store 2 arrays with Balls:
        //-The fixedTrianglePositions Array will store all the balls
        // with a fixed position (whiteball, 1-ball, 8-ball, and the balls
        // in the lower-left and lower-right corner of the triangle)
        //-The trianglePosition Array will store all the remaining balls which
        // don't need a fixed position
        var trianglePositions: Array<(x: Float, z: Float)> = Array()
        var fixedTrianglePositions: Array<(x: Float, z: Float)> = Array()
        
        let triangleCalcResult = Calculations.getInstance().calculateBallTrianglePositions(ballRadius: self.balls[0].getRadius())
        trianglePositions = triangleCalcResult.trianglePos
        fixedTrianglePositions = triangleCalcResult.fixedPos
        
        //Fixed indices of every Ball which needs a fixed position
        let oneBallIndex = 0
        let eightBallIndex = 1
        let lowerLeftBallIndex = 2
        let lowerRightBallIndex = 3
        
        var lowerLeftFilled = false
        var lowerRightFilled = false
        
        //See Documentation file (1.0) for Initial Ball Positions
        //Place fixed Balls
        for ball in self.balls {
            
            //Place fixed Top Triangle Ball (1-ball)
            if ball.getNumber() == 1{
                ball.setPosition(x: fixedTrianglePositions[oneBallIndex].x, y: ball.getPosition().y, z: fixedTrianglePositions[oneBallIndex].z)
                continue
            }
            
            //Place 8-ball in the middle of Triangle
            if ball.getNumber() == 8{
                ball.setPosition(x: fixedTrianglePositions[eightBallIndex].x, y: ball.getPosition().y, z: fixedTrianglePositions[eightBallIndex].z)
                continue
            }
            
            //Place whiteball on its own correct position
            if ball.getNumber() == 0{
                
                if let lastPos = fixedTrianglePositions.last{
                    ball.setPosition(x: lastPos.x, y: ball.getPosition().y, z: lastPos.z)
                }else{
                    print("NO position for whiteball!??")
                }
                continue
            }
            
            //Place a Full Ball in Lower Left Corner of Triangle
            if ball.getNumber() != 1 && ball.getNumber() != 8
                && ball.getType() == BallType.FullBall && lowerLeftFilled == false {
                ball.setPosition(x: fixedTrianglePositions[lowerLeftBallIndex].x, y: ball.getPosition().y, z: fixedTrianglePositions[lowerLeftBallIndex].z)
                
                lowerLeftFilled = true
                continue
            }
            
            //Place a Half Ball in Lower Right Corner of Triangle
            if ball.getNumber() != 1 && ball.getNumber() != 8
                && ball.getType() == BallType.HalfBall && lowerRightFilled == false {
                ball.setPosition(x: fixedTrianglePositions[lowerRightBallIndex].x, y: ball.getPosition().y, z: fixedTrianglePositions[lowerRightBallIndex].z)
                
                lowerRightFilled = true
                continue
            }
            
            //Generate a random index from the trianglePositions Array
            let randomTriPosIndex = Int(arc4random_uniform(UInt32(trianglePositions.count)))
            
            //Get random position from the trianglePositions Array
            let ballX = trianglePositions[randomTriPosIndex].x
            let ballZ = trianglePositions[randomTriPosIndex].z
            trianglePositions.remove(at: randomTriPosIndex)
            
            //Set Ball positions with random position
            ball.setPosition(x: ballX, y: ball.getPosition().y, z: ballZ)
            
            //Reset Ball Potted properties
            ball.setBallPotted(isPotted: false)
            ball.setPottedHole(newHole: nil)
        }
    }
}
