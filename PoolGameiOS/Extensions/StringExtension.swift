//
//  StringExtension.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 06-08-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import Foundation

extension String {
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
}
