//
//  IntExtensions.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 18-07-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import Foundation

extension Int {
    func randomInt(min: Int, max: Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
}


