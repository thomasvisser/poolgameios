//
//  ArrayExtensions.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 06-08-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import Foundation

extension Array {
    func checkDiff(checkArray: Array<Ball>) -> Array<Ball>{
        
        var diffArray: Array<Ball> = Array() as! Array<Ball>
        
        for topLitem in self {
    
            if checkArray.contains(where: { ball in
                if ball.getName() == (topLitem as! Ball).getName() { return true }
                else{ return false }
            }){
                continue
            }else{
                diffArray.append(topLitem as! Ball)
            }
        }
        
        return diffArray
    }
}
