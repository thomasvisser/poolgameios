//
//  CustomHoldButton.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 30-07-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import UIKit
import SpriteKit

class CustomHoldButton: SKNode {

    private var defaultButton: SKSpriteNode!
    private var pressedButton: SKSpriteNode!
    var action: (Bool) -> Void
    
    private var handleButtonAction = false
    
    init(defaultButImage: String, pressedButImage: String, buttonAction: @escaping (Bool) -> Void){

        self.defaultButton = SKSpriteNode(imageNamed: defaultButImage)
        self.pressedButton = SKSpriteNode(imageNamed: pressedButImage)
        
        self.action = buttonAction
        
        super.init()
        
        //Disable pressed button upon initialization
        self.pressedButton.isHidden = true
        self.defaultButton.isHidden = false
        
        self.isUserInteractionEnabled = true
        
        //Add buttons to CustomButton Node (self)
        self.addChild(self.defaultButton)
        self.addChild(self.pressedButton)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init coder(): not implemented!")
    }
    
    func setSize(newSize: CGSize){
        
        self.defaultButton.size = newSize
        self.pressedButton.size = newSize
    }
    
    func getSize() -> CGSize {
        return self.defaultButton.size
    }
    
    /**
    Enables the Button Nodes for interaction
    */
    func enableButtons(){
        self.defaultButton.isHidden = false
        self.defaultButton.isUserInteractionEnabled = false
        
        self.pressedButton.isHidden = true
        self.pressedButton.isUserInteractionEnabled = false
    }
    
    /**
    Disablesthe Button Nodes for interaction
    */
    func disableButtons() {
        self.defaultButton.isHidden = true
        self.defaultButton.isUserInteractionEnabled = true
        
        self.pressedButton.isHidden = false
        self.pressedButton.isUserInteractionEnabled = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.pressedButton.isHidden = false
        self.defaultButton.isHidden = true
        
        guard let touchLocation = touches.first?.location(in: self) else {return}
        
        if self.defaultButton.contains(touchLocation){
            
            self.handleButtonAction = true
            self.action(true)
        }
        
        //Sent a notification to inform that hudInteraction has started
        NotificationCenter.default.post(name: Global.HUDNotificName, object: nil, userInfo: ["hudInteraction": true])
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.pressedButton.isHidden = true
        self.defaultButton.isHidden = false
        
        self.handleButtonAction = false
        self.action(false)
        
        //Sent a notification to inform that hudInteraction has ended
        NotificationCenter.default.post(name: Global.HUDNotificName, object: nil, userInfo: ["hudInteraction": false])
    }
}
