//
//  OverlayScene.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 25-07-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import UIKit
import SpriteKit

protocol HudInteractionDelegate {
    func switchPointOfView(state: String)
}

private enum ZoomState: String {
    case twoD = "2D"
    case threeD = "3D"
}

class OverlayScene: SKScene {
    
    var hudScene: SKScene!
    
    //Scene Nodes
    private var zoomButton: CustomButton!
    private var veloButton: CustomHoldButton!
    private var fireButton: CustomButton!
    private var velocityLabel: SKLabelNode!
    private var infoLabel: SKLabelNode!
    
    private var infoLabelData: Array<String> = Array()
    
    private var velocityTimer: Timer!
    private var infoLabelTimer: Timer!
    
    private var currentZoomState: ZoomState!
    
    private var buttonsDisabledDueToPlayedRound = false
    
    var overlayDelegate: HudInteractionDelegate?
    
    override init(size: CGSize) {
        super.init(size: size)
        
        self.hudScene = SKScene(size: size)
        
        self.setupNodes()
        
        self.currentZoomState = ZoomState.threeD
        
        //Setup Observer to listen for changes for the infoLabel and velocity Label
        NotificationCenter.default.addObserver(self, selector: #selector(updateInfoLabelData(notification:)), name: Global.LabelNotificName, object: nil)
        
        //Setup Observer to listen for changes regarding the state of the HUD buttons
        NotificationCenter.default.addObserver(self, selector: #selector(updateButtonState(notification:)), name: Global.RoundPlayedName, object: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init coder(): not implemented!")
    }
    
    private func setupNodes(){
        
        //Button properties
        let buttonSize = size.width/8
        let buttonMargin = size.width/200
        let labelMargin = size.width/20
        
        //Setup Zoom Button to switch between 3D and 2D views
        self.zoomButton = CustomButton(defaultButImage: "2DImage", pressedButImage: "2DImagePressed", defaultBut2Image: "3DImage", pressedBut2Image: "3DImagePressed", buttonAction: zoomButtonClicked(_:))
        self.zoomButton.setSize(newSize: CGSize(width: buttonSize, height: buttonSize))
        self.zoomButton.position = CGPoint(x: size.width - self.zoomButton.getSize().width - buttonMargin, y: self.zoomButton.getSize().height + buttonMargin)
        
        //Setup Stick Button to set velocity of the white ball
        self.veloButton = CustomHoldButton(defaultButImage: "StickImage", pressedButImage: "StickImagePressed", buttonAction: stickButtonPressed(_:))
        self.veloButton.setSize(newSize: CGSize(width: buttonSize, height: buttonSize))
        self.veloButton.position = CGPoint(x: self.veloButton.getSize().width + buttonMargin, y: self.zoomButton.getSize().height + buttonMargin)
        
        //Setup velocity label to show the velocity set by the velocity button
        self.velocityLabel = SKLabelNode(fontNamed: ".SFUIText-Medium")
        self.velocityLabel.text = "0"
        self.velocityLabel.color = UIColor.white
        self.velocityLabel.horizontalAlignmentMode = .left
        self.velocityLabel.position = CGPoint(x: (self.veloButton.getSize().width/2) + buttonMargin,
                                              y: self.hudScene.size.height - self.velocityLabel.frame.height - labelMargin)
        
        //Setup info label to display information to the end user
        self.infoLabel = SKLabelNode(fontNamed: ".SFUIText-Medium")
        self.infoLabel.fontSize = 20
        self.infoLabel.text = LocalizationHelper.getInstance().localizePlayerTurnMessage(playerName: Game.getInstance().getPlayer1Name())
        self.infoLabel.color = UIColor.white
        self.infoLabel.horizontalAlignmentMode = .left
        self.infoLabel.position = CGPoint(x: size.width/3, y: self.hudScene.size.height - self.velocityLabel.frame.height - labelMargin)
    
        //Setup Fire Button to fire the whiteBall
        // Determine aspect ratio
        let fireImage: UIImage = UIImage(named: "FireImage")!
        let fireAspectR = fireImage.size.height / fireImage.size.width
        let fireButtonSize = buttonSize * 1.2
        let fireButtonMargin: CGFloat = 40
        
        self.fireButton = CustomButton(defaultButImage: "FireImage", pressedButImage: "FireImagePressed", buttonAction: self.fireButtonPressed(_:))
        self.fireButton.setSize(newSize: CGSize(width: fireButtonSize, height: fireButtonSize * fireAspectR))
        self.fireButton.position = CGPoint(x: (self.hudScene.size.width/2), y: self.fireButton.getSize().height - fireButtonMargin)
                
        //Add nodes to the scene
        self.hudScene.addChild(self.zoomButton)
        self.hudScene.addChild(self.veloButton)
        self.hudScene.addChild(self.fireButton)
        self.hudScene.addChild(self.velocityLabel)
        self.hudScene.addChild(self.infoLabel)
    }
    
    func disableHudElements(){
        self.veloButton.disableButtons()
        self.zoomButton.disableButtons()
        self.fireButton.disableButtons()
    }
    
    func enableHudElements(){
        self.veloButton.enableButtons()
        self.zoomButton.enableButtons()
        self.fireButton.enableButtons()
    }
    
    /**
    ZoomButton Action Handler
     - parameters: firstStateEnabled: boolean value to indicite whether
        the button is in its first state or in its second state. Different states
        are used to switch between the different images when pressed.
    */
    private func zoomButtonClicked(_ firstStateEnabled: Bool){
        
        if firstStateEnabled {
            //Change camera pointOfView to 3D overview
            self.overlayDelegate?.switchPointOfView(state: "3D")
            
            if !self.buttonsDisabledDueToPlayedRound {
                //Enable velocity Button and fire Button
                self.veloButton.enableButtons()
                self.fireButton.enableButtons()
            }
            
            self.currentZoomState = ZoomState.threeD
            
        }else {
            //Change camera pointOfView to 2D overview
            self.overlayDelegate?.switchPointOfView(state: "2D")
            
            //Disable velocity Button and fire Button
            self.veloButton.disableButtons()
            self.fireButton.disableButtons()
            
            self.currentZoomState = ZoomState.twoD
        }
    }
    
    /**
    StickButton Action Handler
     - parameters:
        - startVeloTimer: boolean that indicate wheter a timer needs to be started,
            to check for velocity updates
    */
    private func stickButtonPressed(_ startVeloTimer: Bool){
        
        if startVeloTimer{
            
            //Setup Timer to initiate an update loop to increment and decrement the velocity
            self.velocityTimer = Timer.scheduledTimer(timeInterval: 0.03, target: self, selector: #selector(updateVelocity(_:)), userInfo: nil, repeats: true)
        }else{
            
            if self.velocityTimer != nil{
                self.velocityTimer.invalidate()
                self.velocityTimer = nil
            }
        }
    }
    
    /**
    FireButton Action Handler
     - parameters:
        - fireButtonPressed: *unused boolean implementation!*
    */
    private func fireButtonPressed(_ : Bool){
        Game.getInstance().fireWhiteBall()
    }

    /**
    Updates the velocity of the whiteBall and updates the HUD label accordingly
     - parameters:
        - timer: the Timer Object which called this function
    */
    @objc private func updateVelocity(_ timer: Timer){
        let newVelocity = Game.getInstance().updateVelocity()
        
        //Update the velocity Label, with the new velocity of the whiteBall
        DispatchQueue.main.async {
            self.velocityLabel.text = "\(Float(round(10*newVelocity)/10))"
        }
    }
    
    /**
    Updates the data model Object for text to display in the infoLabel. This function can also start displaying the data.
    - parameters:
        - notification: the Notification which called this function, and which contains the message to display
    */
    @objc private func updateInfoLabelData(notification: Notification){
        
        guard let userInfo = notification.userInfo else { print("No UserInfo updateInfoLabelData@OverlayScene"); return}
        
        for data in userInfo {
            let key = data.key as? String
            
            guard let tempKey = key else { print("No key data updateInfoLabelData@OverlayScene)"); continue}
            
            if tempKey == "infoLabelText"{
                
                guard let tempValue = data.value as? String else { print("No value data updateInfoLabelData@OverlayScene"); continue}

                //Update Info Label Text
                self.infoLabelData.append(tempValue)

            }else if tempKey == "veloLabelValue" {
                
                guard let tempValue = data.value as? String else { print("No value data updateInfoLabelData@OverlayScene"); continue}
                
                //Update Velocity Label Value
                self.velocityLabel.text = tempValue
                
            }else if tempKey == "displayInfoText"{
                
                guard let tempValue = data.value as? Bool else { print("No value data updateInfoLabelData@OverlayScene"); continue}
                
                //if true display all the data
                if tempValue {
                    self.infoLabelTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(updateInfoLabel(_:)), userInfo: nil, repeats: true)
                }
            }
        }
    }
    
    /**
    Updates the text of the infoLabel. It gets its data from the infoLabel data model.
    Due to the timer which calls this function, data will update every 2 seconds
    - parameters:
        - timer: the Timer Object which called this function
    */
    @objc private func updateInfoLabel(_ timer: Timer){
        
        let displayData = self.infoLabelData.first
        
        DispatchQueue.main.async {
            self.infoLabel.text = displayData
        }
        
        if self.infoLabelData.count > 0 {
            self.infoLabelData.removeFirst()
        }
        
        //If there is no more data to display from the model, invalidate the timer.
        if self.infoLabelData.count == 0 {
            
            if self.infoLabelTimer != nil{
                self.infoLabelTimer.invalidate()
                self.infoLabelTimer = nil
                
                //Send a notification to the HUD to inform that the ball has been played
                NotificationCenter.default.post(name: Global.RoundPlayedName, object: nil, userInfo: ["disableButtons": false])
            }
        }
    }
    
    /**
    Updates the State of the HUD Buttons. It enables or disables the buttons
     regarding the data provided by the incoming message.
    - parameters:
        - notification: the Notification which called this function, and which contains the data
        determiens the state of the butons
    */
    @objc private func updateButtonState(notification: Notification){
        
        guard let userInfo = notification.userInfo else { print("No UserInfo updateButtonState@OverlayScene"); return}
        
        for data in userInfo {
            let key = data.key as? String
            
            guard let tempKey = key else { print("No key data updateButtonState@OverlayScene)"); continue}
            
            if tempKey == "disableButtons"{
                
                guard let tempValue = data.value as? Bool else { print("No value data updateButtonState@OverlayScene"); continue}
                
                //Disable all the buttons when "disableButtons" message says true, otherwise
                // enable the buttons
                if tempValue {
                    
                    //Disable velocity Button and fire Button
                    self.veloButton.disableButtons()
                    self.fireButton.disableButtons()
                    
                    self.buttonsDisabledDueToPlayedRound = true
                }else {
                    
                    if self.currentZoomState == .threeD{
                        //Disable velocity Button and fire Button
                        self.veloButton.enableButtons()
                        self.fireButton.enableButtons()
                    }
                    
                    self.buttonsDisabledDueToPlayedRound = false
                }
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Global.LabelNotificName, object: nil)
    }
}
