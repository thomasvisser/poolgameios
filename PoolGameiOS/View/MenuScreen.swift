//
//  MenuScreen.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 08-08-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import UIKit
import SpriteKit

/**
This SpriteKit Node Class represents the Game Menu View.
This view will be shown when the game has ended, and it will show information
why the player has won/lost. It also provides controls to return to the home screen
and to restart the game.
*/
class MenuScreen: SKSpriteNode {
    
    //Menu Screen Nodes
    private var replayButton: CustomButton!
    private var homeScreenButton: CustomButton!
    private var infoLabel: SKMultilineLabel!
    var infoLabelText: String!
    
    //Store reference to the parent node which int this case will be the HUD Node
    private var parentNode: SKScene!
    
    var delegate: GameMenuDelegate?
    
    init(parentNode: SKScene, infoLabelText: String) {
        
        let background = SKTexture(imageNamed: "GameMenuBackground")
        let size = CGSize(width: 300, height: 350)
        
        self.parentNode = parentNode
        self.infoLabelText = infoLabelText
        
        super.init(texture: background, color: .clear, size: size)
        
        self.setupGameMenu()
        self.setupNodes()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init coder(): not implemented!")
    }
    
    private func setupGameMenu(){
        self.position = CGPoint(x: self.parentNode.size.width / 2, y: self.parentNode.size.height / 2)
    }
    
    private func setupNodes(){
        
        //Setup infoLabel
        self.infoLabel = SKMultilineLabel(text: self.infoLabelText, labelWidth: 250, pos: CGPoint(x: 0, y: self.position.y - self.parentNode.size.width + self.size.height/2), fontName: ".SFUIText-Medium", fontSize: 20, fontColor: UIColor.white, leading: 25, alignment: SKLabelHorizontalAlignmentMode.center, shouldShowBorder: false)
        
        //Setup Replay Button
        self.replayButton = CustomButton(defaultButImage: "ReplayImage", pressedButImage: "ReplayImagePressed", buttonAction: self.replayButtonPressed(_:))
        self.replayButton.setSize(newSize: CGSize(width: 90, height: 55))
        self.replayButton.position = CGPoint(x: -(self.size.width / 4), y: -80)
        
        //Setup HomeScreen Button
        self.homeScreenButton = CustomButton(defaultButImage: "HomeScreenImage", pressedButImage: "HomeScreenImagePressed", buttonAction: self.homeScreenButtonPressed(_:))
        self.homeScreenButton.setSize(newSize: CGSize(width: 90, height: 55))
        self.homeScreenButton.position = CGPoint(x: (self.size.width / 4), y: -80)
        
        self.addChild(self.infoLabel)
        self.addChild(self.replayButton)
        self.addChild(self.homeScreenButton)
    }
    
    @objc private func replayButtonPressed(_ : Bool){
        
        Game.getInstance().resetGame()
        
        self.delegate?.removeGameMenuScreen()
    }
    
    @objc private func homeScreenButtonPressed(_ : Bool){
        
        Game.getInstance().resetGame()
        
        self.delegate?.goToSplashScreen()
    }
}
