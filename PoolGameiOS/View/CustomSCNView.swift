//
//  CustomView.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 26-07-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import UIKit
import SceneKit

class CustomSCNView: SCNView, UIGestureRecognizerDelegate {
    
    var follow_Camera: SCNNode!
    var customCamera: SCNNode!
    
    var interactionWithHUD = false

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //DEBUG: Show statistics of the Game
        //self.showsStatistics = true
        
        //Setup Node Properties
        self.follow_Camera = SCNNode()
        self.follow_Camera.name = "FollowCamera"
        self.follow_Camera.position = SCNVector3(x: 0, y: 4.36, z: 0)
        self.follow_Camera.eulerAngles = SCNVector3(x: ConversionHelper.getInstance().convertDegreeToRad(deg: -45), y: 0, z: 0)
        
        self.customCamera = SCNNode()
        self.customCamera.name = "CustomCamera"
        self.customCamera.position = self.follow_Camera.position + SCNVector3(x: 0, y: -2.18, z: 14.918)
        self.customCamera.eulerAngles = SCNVector3(x: 0, y: 0, z: 0)
        self.customCamera.camera = SCNCamera()
        
        //Add customCamera as child of follow_camera, so if changes were made to
        // the follow_camera, it will affect customCamera also
        self.follow_Camera.addChildNode(self.customCamera)
        
        //Setup Observer to listen for changes from the HUD
        NotificationCenter.default.addObserver(self, selector: #selector(interactedWithHUD), name: Global.HUDNotificName, object: nil)
    }
    
    override init(frame: CGRect, options: [String : Any]? = nil) {
        super.init(frame: frame, options: options)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder): has not been implemented!")
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        guard let location = touch?.location(in: self) else { print("Could not determine touch @touchesMoved"); return }
        
        //Make sure the user touched with 1 finger
        if touches.count == 1{
            
            if !self.interactionWithHUD {
                CameraMovement.getInstance().handleCameraRotation(camera: self.follow_Camera, location: location)
            }
        }
    }
    
    /**
    Notification selector method. This function is used to receive information
    about whether the user interacts with the HUD yes or no. If yes, camera movement
    is not allowed.
     - parameters:
        - notification: the incoming Notification with corresponding data
    */
    @objc public func interactedWithHUD(notification: Notification){
        
        guard let userInfo = notification.userInfo else { print("No UserInfo interactionWithHud@CustomSCNView"); return}
        
        for data in userInfo {
            let key = data.key as? String
            let value = data.value as? Bool
            
            if let tempKey = key, let tempValue = value {
                if tempKey == "hudInteraction"{ self.interactionWithHUD = tempValue }
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Global.HUDNotificName, object: nil)
    }
}
