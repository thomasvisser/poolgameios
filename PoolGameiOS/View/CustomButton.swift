//
//  CustomButton.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 27-07-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import UIKit
import SpriteKit

class CustomButton: SKNode {

    private var defaultButton: SKSpriteNode!
    private var pressedButton: SKSpriteNode!
    var action: (Bool) -> Void
    
    //Image names for the images the button can have
    var firstStateImageName: String!
    var firstStatePressedImageName: String!
    var secondStateImageName: String!
    var secondStatePressedImageName: String!
    
    var firstStateEnabled = false
    
    init(defaultButImage: String, pressedButImage: String,
         defaultBut2Image: String?, pressedBut2Image: String?, buttonAction: @escaping (Bool) -> Void){
        
        self.firstStateImageName = defaultButImage
        self.firstStatePressedImageName = pressedButImage
        self.secondStateImageName = defaultBut2Image
        self.secondStatePressedImageName = pressedBut2Image
        
        self.defaultButton = SKSpriteNode(imageNamed: defaultButImage)
        self.pressedButton = SKSpriteNode(imageNamed: pressedButImage)
                
        self.action = buttonAction
        
        super.init()
        
        //Disable pressed button upon initialization
        self.pressedButton.isHidden = true
        self.defaultButton.isHidden = false
        self.firstStateEnabled = true
        
        self.isUserInteractionEnabled = true
        
        //Add buttons to CustomButton Node (self)
        self.addChild(self.defaultButton)
        self.addChild(self.pressedButton)
    }
    
    convenience init(defaultButImage: String, pressedButImage: String, buttonAction: @escaping (Bool) -> Void){
     
        self.init(defaultButImage: defaultButImage, pressedButImage: pressedButImage, defaultBut2Image: nil, pressedBut2Image: nil, buttonAction: buttonAction)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init coder(): not implemented!")
    }
    
    func setSize(newSize: CGSize){
        
        self.defaultButton.size = newSize
        self.pressedButton.size = newSize
    }
    
    func getSize() -> CGSize {
        return self.defaultButton.size
    }
    
    /**
     Enables the Button Nodes for interaction
     */
    func enableButtons(){
        self.defaultButton.isHidden = false
        self.defaultButton.isUserInteractionEnabled = false
        
        self.pressedButton.isHidden = true
        self.pressedButton.isUserInteractionEnabled = false
    }
    
    /**
     Disablesthe Button Nodes for interaction
     */
    func disableButtons() {
        self.defaultButton.isHidden = true
        self.defaultButton.isUserInteractionEnabled = true
        
        self.pressedButton.isHidden = false
        self.pressedButton.isUserInteractionEnabled = true
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.pressedButton.isHidden = false
        self.defaultButton.isHidden = true
        
        //Sent a notification to inform that hudInteraction has started
        NotificationCenter.default.post(name: Global.HUDNotificName, object: nil, userInfo: ["hudInteraction": true])
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.pressedButton.isHidden = true
        self.defaultButton.isHidden = false
        
        guard let touchLocation = touches.first?.location(in: self) else {return}
        
        if self.defaultButton.contains(touchLocation){
            
            self.checkImageState()
            self.action(self.firstStateEnabled)
        }
        
        //Sent a notification to inform that hudInteraction has ended
        NotificationCenter.default.post(name: Global.HUDNotificName, object: nil, userInfo: ["hudInteraction": false])
    }
    
    /**
    Checks the state of the Button, and sets the corresponding
    Button Image for every State.
    */
    private func checkImageState(){
        
        if self.firstStateEnabled{
            if let secondImage = self.secondStateImageName, let secondPressedImage = self.secondStatePressedImageName {
                
                self.defaultButton.texture = SKTexture(imageNamed: secondImage)
                self.pressedButton.texture = SKTexture(imageNamed: secondPressedImage)
                
                self.firstStateEnabled = false
            }
        }
        else {
            self.defaultButton.texture = SKTexture(imageNamed: self.firstStateImageName)
            self.pressedButton.texture = SKTexture(imageNamed: self.firstStatePressedImageName)
            
            self.firstStateEnabled = true
        }
    }
}
