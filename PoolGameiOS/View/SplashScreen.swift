//
//  SplashScreen.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 10-08-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import UIKit

/**
SplashScreen class represents the Splash Screen of the game.
Here the players can enter the usernames for player 1 and player 2
and eventually start the game.
*/
class SplashScreen: UIViewController{
    
    var backgroundImage: UIImageView!
    var infoLabel: UILabel!
    var playButton: UIButton!
    
    var userName1TextField: UITextField!
    var userName2TextField: UITextField!
    var userName1Label: UILabel!
    var userName2Label: UILabel!
    
    var userNamePlayer1: String!
    var userNamePlayer2: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupControls()
        
        //Setup Observer to listen for a signal from the game
        // to clear the player name fields
        NotificationCenter.default.addObserver(self, selector: #selector(resetFields(notification:)), name: Global.SplashScreenNotificName, object: nil)
    }
    
    private func setupControls(){
        
        self.backgroundImage = UIImageView(frame: self.view.frame)
        self.backgroundImage.image = UIImage(named: "SplashScreenBackground")
        self.backgroundImage.contentMode = .scaleAspectFill
        self.backgroundImage.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        
        //Setup the Info Label
        self.infoLabel = UILabel()
        self.infoLabel.text = "Welcome to this 8-ball Pool Game!\n First enter your usernames.\n Then click play to start pooling!"
        self.infoLabel.numberOfLines = 3
        self.infoLabel.textAlignment = .center
        self.infoLabel.textColor = .black
        self.infoLabel.font = UIFont(name: ".SFUIText-Medium", size: 20)
        self.infoLabel.frame = CGRect(x: 0, y: self.view.bounds.height / 4, width: self.view.bounds.width, height: self.infoLabel.intrinsicContentSize.height)
        
        //Get some setup properties for PlayButton
        let playButtonText = UILabel()
        playButtonText.font = UIFont(name: ".SFUIText-Bold", size: 20)
        playButtonText.text = "PLAY!"
        let playButtonTextWidth = playButtonText.intrinsicContentSize.width
        let playButtonTextHeight = playButtonText.intrinsicContentSize.height
        
        //Setup the Play Button
        let playButtonSize = CGSize(width: playButtonTextWidth, height: playButtonTextHeight)
        self.playButton = UIButton(frame: CGRect(x: self.view.bounds.width/2 - playButtonSize.width/2, y: self.view.bounds.height / 1.5, width: playButtonSize.width, height: playButtonSize.height))
        self.playButton.setTitle(playButtonText.text, for: .normal)
        self.playButton.titleLabel?.font = playButtonText.font
        self.playButton.titleLabel?.textColor = .black
        self.playButton.addTarget(self, action: #selector(self.playButtonPressed(_:)), for: .touchUpInside)
        
        self.view.addSubview(self.backgroundImage)
        self.view.addSubview(self.infoLabel)
        self.view.addSubview(self.playButton)
        
        self.setupUserNameFields()
        
    }
    
    private func setupUserNameFields(){
        
        let userNameTextFieldSize = CGSize(width: 150, height: 25)
        
        //Setup Fixed Username Labels
        self.userName1Label = UILabel()
        self.userName1Label.text = "Player 1:"
        self.userName1Label.textAlignment = .center
        self.userName1Label.textColor = .black
        self.userName1Label.font = UIFont(name: ".SFUIText-Medium", size: 20)
        self.userName1Label.frame = CGRect(x: self.view.bounds.width / 4, y: self.view.bounds.height / 2.2, width: self.userName1Label.intrinsicContentSize.width, height: self.userName1Label.intrinsicContentSize.height)
        
        //Setup Input Field Player 1
        self.userName1TextField = UITextField(frame:
            CGRect(x: self.view.bounds.width / 2 + 10, y: self.view.bounds.height / 2.2, width: userNameTextFieldSize.width, height: userNameTextFieldSize.height))
        self.userName1TextField.placeholder = "Username Player 1"
        self.userName1TextField.backgroundColor = .white
        self.userName1TextField.font = UIFont(name: ".SFUIText-Medium", size: 15)
        self.userName1TextField.keyboardType = .default
        self.userName1TextField.keyboardAppearance = .dark
        self.userName1TextField.returnKeyType = .done
        self.userName1TextField.clearButtonMode = .whileEditing
        self.userName1TextField.delegate = self
        
        //Setup Fixed Username Labels
        self.userName2Label = UILabel()
        self.userName2Label.text = "Player 2:"
        self.userName2Label.textAlignment = .center
        self.userName2Label.textColor = .black
        self.userName2Label.font = UIFont(name: ".SFUIText-Medium", size: 20)
        self.userName2Label.frame = CGRect(x: self.view.bounds.width / 4, y: self.view.bounds.height / 2.2 + userNameTextFieldSize.height + 20, width: self.userName2Label.intrinsicContentSize.width, height: self.userName1Label.intrinsicContentSize.height)
        
        //Setup Input Field Player 2
        self.userName2TextField = UITextField(frame:
            CGRect(x: self.view.bounds.width / 2 + 10, y: self.view.bounds.height / 2.2 + userNameTextFieldSize.height + 20, width: userNameTextFieldSize.width, height: userNameTextFieldSize.height))
        self.userName2TextField.placeholder = "Username Player 2"
        self.userName2TextField.backgroundColor = .white
        self.userName2TextField.font = UIFont(name: ".SFUIText-Medium", size: 15)
        self.userName2TextField.keyboardType = .default
        self.userName2TextField.keyboardAppearance = .dark
        self.userName2TextField.returnKeyType = .done
        self.userName2TextField.clearButtonMode = .whileEditing
        self.userName2TextField.delegate = self
        
        self.view.addSubview(self.userName1TextField)
        self.view.addSubview(self.userName2TextField)
        self.view.addSubview(self.userName1Label)
        self.view.addSubview(self.userName2Label)
        
    }
    
    @objc private func playButtonPressed(_ button: UIButton){
        
        //Checks if userNames are empty
        if userNamePlayer1 == "" || userNamePlayer1 == nil {
            userNamePlayer1 = "Player 1"
        }
        
        if userNamePlayer2 == "" || userNamePlayer2 == nil {
            userNamePlayer2 = "Player 2"
        }
        
        //Sends the usernames to the Game Object, to store them for use in the
        // rest of the game
        Game.getInstance().setPlayer1Name(userName: self.userNamePlayer1)
        Game.getInstance().setPlayer2Name(userName: self.userNamePlayer2)
        Game.getInstance().updateCurrentPlayer()
        
        //Segue from the current Splash Screen to the Main Game Scene
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let gameSceneViewController = storyBoard.instantiateViewController(withIdentifier: "GameViewController") as! GameViewController
        gameSceneViewController.modalPresentationStyle = .fullScreen
        self.present(gameSceneViewController, animated: true, completion: nil)
    }
    
    @objc private func resetFields(notification: Notification){
        
        guard let userInfo = notification.userInfo else { print("No UserInfo resetFields@SplashScreen"); return}
        
        for data in userInfo {
            let key = data.key as? String
            
            guard let tempKey = key else { print("No key data resetFields@SplashScreen"); return}
            
            if tempKey == "clearFields" {
                
                guard let tempValue = data.value as? Bool else { print("No value data resetFields@SplashScreen"); return}
                
                if tempValue{
                    self.userName1TextField.text = nil
                    self.userName2TextField.text = nil
                    
                    print("Cleared SplashSCREEN FIELDS!")
                }
            }
        }
    }
}

/**
 Implementation methods of the UITextFieldDelegate. This is used to obtain
 text from the textFields, and to register interaction with it.
 */
extension SplashScreen: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.placeholder == "Username Player 1"{
            self.userNamePlayer1 = textField.text
        }else{
            self.userNamePlayer2 = textField.text
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return false
    }
}
