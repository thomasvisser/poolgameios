//
//  Calculations.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 21-07-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import Foundation
import SceneKit

public class Calculations {
    
    private static let instance = Calculations()
    
    private var switchToUnevenZ = true
    private var switchToEvenX = true
    private var firstZDirCalcSet = false
    private var firstXDirCalcSet = false
    private var zDirectionCalc: Float!
    private var xDirectionCalc: Float!
    
    public static func getInstance() -> Calculations{
        return instance
    }
    
    private init(){}

    //IN BETA!
    /*public func calcIfBallPosTouchesWall(wall: SCNNode, pos: (x: Float, y: Float)){
        
        let holeWallXMin = (wall.geometry?.boundingBox.min.x)!
        let holeWallXMax = (wall.geometry?.boundingBox.max.x)!
        
        let holeWallXPosMin = wall.position.x + (wall.geometry?.boundingBox.min.x)!
        let holeWallXPosMax = wall.position.x + (wall.geometry?.boundingBox.max.x)!
        let hypotenuse = holeWallXPosMax - holeWallXPosMin
        
        let pos = wall.position
        
        //Debug
        let holeWallZ = wall.position.z
        let boundingBox = wall.boundingBox
        
        //Determine distance from X minimum and X maximum (World 3D Space) points of the Wall
        let rotation = wall.rotation
        //let degrees = ConversionHelper.getInstance().convertRadToDegree(rad: rotation.w)
        let xDiff = cos(rotation.w) * hypotenuse
        print("cos \(cos(rotation.w))")
        
        //Determine distance from Z minimum and Z maximum (World 3D Space) points of the Wall
        let zDiff = sin(rotation.w) * hypotenuse
        print("sin \(sin(rotation.w))")
        
        //Needs to return xDiff, zDiff and a boolean if it touches the wall yes or no
    }*/
    
    /**
    Calculates all the positions to place all the 15 balls in a traingle shape
    - parameters:
        - ballRadius: The radius of the balls
    */
    public func calculateBallTrianglePositions(ballRadius: Float) -> (trianglePos: Array<(x: Float, z: Float)>, fixedPos: Array<(x: Float, z: Float)>){
        
        var trianglePositions: Array<(x: Float, z: Float)> = Array()
        var fixedTrianglePositions: Array<(x: Float, z: Float)> = Array()
        
        let amountOfRows = 5
        
        //Increases by one if the row counts upwards
        var amountBallsOnRow = 1
        
        let ballDistance = (ballRadius * 2)
        var position: (x: Float, z: Float) = (x: 0, z: -4.369)
        
        var extractZ: Float = 0
        
        //Fill trianglePositions Array with alle the possible Ball positions a Ball
        // can have within the Triangle
        //Also fill fixedTrianglePositions with the fixed ball positions
        for currentRowIndex in 0..<amountOfRows{
            
            let percentageDiff = ballDistance * 0.14 //14% of total diff
            extractZ = -(ballDistance - percentageDiff) * Float(currentRowIndex)
            
            position.x = Float(ballRadius * Float(currentRowIndex))
            position.z = Float(-4.369 + extractZ)
            
            for ballRowIndex in 0..<amountBallsOnRow {
                
                //Check for fixed ball positions
                if currentRowIndex == 0 || (currentRowIndex == 2 && ballRowIndex == 1) ||
                    (currentRowIndex == 4 && (ballRowIndex == 0 || ballRowIndex == 4)){
                    
                    fixedTrianglePositions.append((x: position.x, z: position.z))
                }else{
                    trianglePositions.append((x: position.x, z: position.z))
                }
                position.x = position.x - ballDistance
            }
            
            amountBallsOnRow = amountBallsOnRow + 1
        }
        
        //Add whiteBall to fixed positions Array
        fixedTrianglePositions.append((x: 0, z: 2.871))
        
        return(trianglePos: trianglePositions, fixedPos: fixedTrianglePositions)
    }
    
    /**
    Calculates the new position of the camera based on the position of the ball.
    It is intended the Camera follows the Ball position
    */
    public func calculateCameraPosition() -> SCNVector3?{
        
        //Get whiteBall position to follow
        guard let ballPosition = Balls.getInstance().getBallPosition(withName: "whiteBall") else { print("No ball position @calculateCameraPosition"); return nil}

        let cameraPosition = SCNVector3(x: ballPosition.x, y: ballPosition.y + 12, z: ballPosition.z + 6)
        
        return cameraPosition
    }
    
    private var allowedToSwitchZDirLower = false
    private var allowedToSwitchZDirHigher = false
    private var previousMod: Float = 0
    private var latestMod: Float = 0
    private var latestXMod: Float = 0
    
    /**
    Calculates the new direction for the whiteBall, based on the rotation of the main
    camera in the game. It is intended to move the ball in the direction of the Camera direction.
    - parameters:
      - eulerY: the Euler y rotation value, to calculate the direction from
    */
    public func calculateWhiteBallDirection(eulerY: Float) -> SCNVector3{
        
        let degreeY = ConversionHelper.getInstance().convertRadToDegree(rad: eulerY)
        
        //Get modolo calculations used for the inverse direction calculations.
        // Using this calculations, it can be recognized when the angle is past any 180 degrees
        latestMod = degreeY.truncatingRemainder(dividingBy: 180)
        
        print("previous Mod: \(previousMod)")
        print("latest Mod: \(latestMod)")
        
        //If difference between the latest angle and the last angle is > 150, it means
        // the angle has crossed the 180 degree point, in which case the x direction needs te be reverted
        if abs(previousMod - latestMod) > 150 {
            
            print("in > 150")
            if latestMod < 90 {
                latestXMod = 0
            }else if latestMod > 90 {
                latestXMod = 180
            }
            
            allowedToSwitchZDirLower = true
            allowedToSwitchZDirHigher = false
            
            //Reverse the X direction
            if self.xDirectionCalc == -1 { self.xDirectionCalc = 1 }else{ self.xDirectionCalc = -1 }
        }
        
        print("latest X Mod: \(latestXMod)")
        
        if abs(latestXMod - latestMod) > 90 && abs(latestXMod - latestMod) < 100 {
            
            print("in > 90 && < 100")
            print("ZDirLower \(allowedToSwitchZDirLower)")
            if allowedToSwitchZDirLower {
                
                //Reverse the Z direction
                if self.zDirectionCalc == -1 { self.zDirectionCalc = 1 }else{ self.zDirectionCalc = -1 }
                
                allowedToSwitchZDirLower = false
                allowedToSwitchZDirHigher = true
            }
        }else if abs(latestXMod - latestMod) < 90 {
            
            print("in < 90")
            print("ZDirHigher \(allowedToSwitchZDirHigher)")
            if allowedToSwitchZDirHigher {
                
                //Reverse the Z direction
                if self.zDirectionCalc == -1 { self.zDirectionCalc = 1 }else{ self.zDirectionCalc = -1 }
                
                allowedToSwitchZDirHigher = false
                allowedToSwitchZDirLower = true
            }
        }
        
        previousMod = latestMod
        
        //Final direction
        var directionFinal: SCNVector3!
        
        //Calculate modulation result of the amount of degrees. This is used to determine
        // if the amount of degrees is of a standard angle yes or no.
        let tempDirX = degreeY / -90
        let tempDirZ = degreeY / -90
        
        print("tempDirX: \(tempDirX) tempDirZ: \(tempDirZ)")
        
        //If amount of degrees is > 90 and < 270, tempDirZ will be between -1.5 and -0.5,
        // which means it needs to reverse the z-direction
        var modZ: Float!
        var finalDirZ: Float!
        if tempDirZ <= -1 || tempDirZ >= 1{
            
            modZ = tempDirZ.truncatingRemainder(dividingBy: 1)
            
            if tempDirZ <= -1 {
                
                //Set very first calculation direction value
                if !self.firstZDirCalcSet{
                    self.zDirectionCalc = -1
                    self.allowedToSwitchZDirLower = true
                    self.firstZDirCalcSet = true
                }
                
                if tempDirZ.truncatingRemainder(dividingBy: -2) > -1{
                    finalDirZ = (1 - abs(modZ)) * self.zDirectionCalc
                }else{
                    finalDirZ = abs(modZ) * zDirectionCalc
                }
            }else if tempDirZ >= 1 {
                
                //Set very first calculation direction value
                if !self.firstZDirCalcSet{
                    self.zDirectionCalc = -1
                    self.allowedToSwitchZDirHigher = true
                    self.firstZDirCalcSet = true
                }
                
                if tempDirZ.truncatingRemainder(dividingBy: 2) < 1 {
                    finalDirZ = (1 - abs(modZ)) * self.zDirectionCalc
                }else{
                    finalDirZ = abs(modZ) * zDirectionCalc
                }
            }
        }else{
            modZ = tempDirZ.truncatingRemainder(dividingBy: -1)
            finalDirZ = -1 + abs(modZ)
        }
        
        print("Mod Z: \(modZ)")
        
        var modX: Float!
        var finalDirX: Float!
        
        //If tempDirX goes past the bounds of the custom direction scale (-1 till 1), make modulo
        // calculations to convert it back to the custom scale.
        if tempDirX <= -1 || tempDirZ >= 1 {
            
            if tempDirX <= -1 {
                
                //Set very first calculation direction value
                if !self.firstXDirCalcSet{
                    self.xDirectionCalc = 1
                    self.firstXDirCalcSet = true
                }
                
                //Calculate remainder of the tempDirX value
                modX = tempDirX.truncatingRemainder(dividingBy: -1)
                
                //Check if tempDirX is an even number, because in that case the direction calculations
                // needs to be altered
                if tempDirX.truncatingRemainder(dividingBy: -2) > -1 {
                    finalDirX = modX * self.xDirectionCalc
                }else {
                    finalDirX = (-1 + abs(modX)) * self.xDirectionCalc
                }
                
            }else if tempDirX >= 1{
                
                //Set very first calculation direction value
                if !self.firstXDirCalcSet{
                    self.xDirectionCalc = 1
                    self.firstXDirCalcSet = true
                }
                
                //Calculate remainder of the tempDirX value
                modX = tempDirX.truncatingRemainder(dividingBy: 1)
                
                //Check if tempDirX is an even number, because in that case the direction calculations
                // needs to be altered
                if tempDirX.truncatingRemainder(dividingBy: 2) < 1 {
                    finalDirX = modX * self.xDirectionCalc
                }else {
                    finalDirX = (1 - abs(modX)) * self.xDirectionCalc
                }
            }
        }else{
            modX = tempDirX
            finalDirX = modX
        }
        
        print("Mod X: \(modZ)")
        
        print("Final Direction X: \(finalDirX)")
        print("Final Direction Z: \(finalDirZ)\n")
        directionFinal = SCNVector3(x: finalDirX, y: 0, z: finalDirZ)
        
        return directionFinal
    }
}
