//
//  UpdateLoop.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 03-07-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import Foundation
import SceneKit

//Extension for the GameViewController to implement the update loop
extension GameViewController {
    
    @objc func updateRenderer(_ timer: Timer) {
    
        //Checl Collisions
        self.checkCollisions()
        self.updatePositions()
        
        //Check Camera movement
        self.updateCameraPosition()
        
        //Check for Game Rounds
        Game.getInstance().checkRoundEnded()
    }
    
    /**
    Checks Physics Collision for the balls
    */
    public func checkCollisions(){
        
        Balls.getInstance().checkCollisions()
        Balls.getInstance().checkBallCollision()
    }
    
    /**
    Updates positions and properties for the balls
    */
    public func updatePositions(){
        
        Balls.getInstance().updateBalls()
    }
    
    public func updateCameraPosition(){
        
        if let ballPos = Balls.getInstance().getBallPosition(withName: "whiteBall"){
            self.sceneView.follow_Camera.position = ballPos
        }
    }
}

//Extension the SCNPhysicsContactDelegate to catch contacts registered by the Physics Engine
//This is used to detect contact between the Balls and the Holes
extension GameViewController: SCNPhysicsContactDelegate {
    
    func physicsWorld(_ world: SCNPhysicsWorld, didBegin contact: SCNPhysicsContact) {
 
        let ballName: String!
        let wallName: String!
        
        if (contact.nodeA.name?.contains("Ball"))!{
            ballName = contact.nodeA.name
            wallName = contact.nodeB.name
        }else{
            ballName = contact.nodeB.name
            wallName = contact.nodeB.name
        }
        
        let CtopWall: SCNVector3 = (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Top).position)!
        let CbottomWall: SCNVector3 = (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Bottom).position)!
        let CleftWall: SCNVector3 = (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Left_Top).position)!
        let CrightWall: SCNVector3 = (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Right_Top).position)!
        
        guard let ballPosition = Balls.getInstance().getBallPosition(withName: ballName) else { return }
        guard let ballRadius = Balls.getInstance().getBallRadius(withName: ballName) else { return }
        
        //Make sure the ball position is past the bounds of the outer walls, before sinking in a Hole
        if ((ballPosition.x + ballRadius*2) <= CleftWall.x) || ((ballPosition.x - ballRadius*2) >= CrightWall.x) ||
            ((ballPosition.z - ballRadius*2) <= CtopWall.z) || ((ballPosition.z + ballRadius*2) >= CbottomWall.z){
            
            Balls.getInstance().setBallDirection(direction: SCNVector3(x: contact.contactNormal.x, y: -1, z: contact.contactNormal.z), withName: ballName)
            
            guard let holeType = ConversionHelper.getInstance().convertToHoleType(holeName: wallName) else {
                print("Could not determine HoleType from name @UpdateLoop!"); return
            }
            
            //Store the Hole the ball is putted into
            Balls.getInstance().setBallPottedHole(potHole: holeType, withName: ballName)
        }
    }
}
