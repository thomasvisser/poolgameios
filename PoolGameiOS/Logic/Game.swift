//
//  Game.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 30-07-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import Foundation
import SceneKit

struct Player {
    enum PlayerNumber {
        case player1
        case player2
    }
    let typePlayer: PlayerNumber
    var typeBall: BallType?
    var name: String!
}

public enum EndGameCondition {
    case normal
    case eightBallEarly
    case eightBallFinal
}

protocol GameMenuDelegate {
    func showGameMenuScreen(withMessage: String)
    func removeGameMenuScreen()
    func goToSplashScreen()
}

/**
Game Class contains all the Game Logic, for the game to work well.
It contains the rules for 8Ball, as well as updating game elements like
velocity and direction.
*/
public class Game {
    
    private static let instance = Game()
    
    //Players
    private var player1 = Player(typePlayer: .player1, typeBall: nil, name: "Player 1")
    private var player2 = Player(typePlayer: .player2, typeBall: nil, name: "Player 2")
    private var currentPlayer: Player!

    //Array which stores all potted Ball Names
    private var allPottedBalls: Array<Ball> = Array()
    private var pottedBallsPlayer1: Array<Ball> = Array()
    private var pottedBallsPlayer2: Array<Ball> = Array()
    
    //Additional
    private var roundPlayed = true
    private var playerPottedOpBalls = false
    private var lastPotHolePlayer1: CollisionHoleType!
    private var lastPotHolePlayer2: CollisionHoleType!
    
    //Delegate of GameMenuDelegate Protocol
    var delegate: GameMenuDelegate?

    //Temporary property which stores the whiteBall Velocity
    private var whiteBallVelo: Float = 0
    private var operationType: String!
    
    private init(){
        
        //Initial Setup when game starts
        self.currentPlayer = self.player1
    }
    
    public static func getInstance() -> Game {
        return instance
    }
    
    func getRoundPlayed() -> Bool{
        return self.roundPlayed
    }
    
    func getPlayer1Name() -> String {
        return self.player1.name
    }
    
    func updateCurrentPlayer() {
        
        if self.currentPlayer.typePlayer == .player1{
            self.currentPlayer = self.player1
        }else{
            self.currentPlayer = self.player2
        }
    }
    
    public func setPlayer1Name(userName: String){
        self.player1.name = userName
    }
    
    public func setPlayer2Name(userName: String){
        self.player2.name = userName
    }
    
    /**
    Updates the velocity of the whiteBall, initiated by the player, who interacts
    with the Velocity button.
    */
    public func updateVelocity() -> Float{
        
        let changeSpeed: Float = 0.1
        
        //Check for the current velocity, to determine if it needs to
        // increment or decrement the velocity (It needs to stay in a range from 0 till 10)
        //Offset 0.1 and 9.9 is used to make sure the velocity will not go out of range
        if self.whiteBallVelo <= 0 + changeSpeed {
            self.operationType = "incr"
        }else if self.whiteBallVelo >= Global.velocitySteps - changeSpeed {
            self.operationType = "decr"
        }
        
        if self.operationType == "incr" {
            self.whiteBallVelo = self.whiteBallVelo + changeSpeed
        }else if self.operationType == "decr" {
            self.whiteBallVelo = self.whiteBallVelo - changeSpeed
        }
        
        return self.whiteBallVelo
    }
    
    /**
     Updates the direction of the whiteBall, initiated by the player, who interacts
     with the Camera.
     */
    public func updateDirection(newDir: SCNVector3){
  
        //Sets a new direction for the whiteBall
        Balls.getInstance().setBallDirection(direction: newDir, withName: "whiteBall")        
    }
    
    /**
    Fires off the whiteBall in its set direction, with its set velocity
    */
    public func fireWhiteBall(){
        
        //Store the current position of the whiteBall as the last position value
        Balls.getInstance().setLatestBallPos(ofBall: "whiteBall")
        
        //Set velocity of whiteBall to temporary velocity value
        Balls.getInstance().setBallVelocity(velocity: self.whiteBallVelo, withName: "whiteBall")
        
        self.roundPlayed = false
        
        //Send a notification to the HUD to inform that the user played the ball
        NotificationCenter.default.post(name: Global.RoundPlayedName, object: nil, userInfo: ["disableButtons": true])
    }
    
    /**
    Checks if a played round has ended yes or no. If all the balls stopped moving, a round
    is considered played.
    */
    public func checkRoundEnded() {
        
        //If a Game Round has not been played yet, check for the requirements for the round
        // to be played
        if !self.roundPlayed {
            
            //If all the balls have a velocity of zero, consider a round as played
            if !Balls.getInstance().checkBallVelocity() {
                self.roundPlayed = true
                
                //Check game rules
                self.checkGameRules()
            }
        }
    }
    
    /**
    Checks the state of the game against the rules of the game.
    It will inform the user about balls potted, or any winners etc.
    */
    private func checkGameRules(){
        
        print("In game rules!!")
        
        //Check for any potted balls
        let tempResult = Balls.getInstance().checkBallsPotted()
        
        //Check the resulting potted balls against the private known potted balls
        let diffPottedBalls = tempResult.checkDiff(checkArray: self.allPottedBalls)
        var playerPottedBalls = false
        self.playerPottedOpBalls = false
        
        for tempBall in diffPottedBalls {
            
            playerPottedBalls = true
            
            //Determine type of Ball the player needs to play with, upon first ball potted
            if self.allPottedBalls.count == 0 {
                self.determinePlayerTypes(firstPBall: tempBall)
            }
            
            //Send message to inform player potted a ball
            let pottedBallMessage = LocalizationHelper.getInstance().localizeBallPottedMessage(playerName: self.currentPlayer.name, ball: tempBall.getName())
            self.sendInfoMessage(with: "infoLabelText", message: pottedBallMessage)
            
            //Check for 8ball potted
            if tempBall.getName() == "eightBall"{
                
                print("Registerd potted eightBall!!")
                
                self.check8ballRules(eightBall: tempBall)
                return
            }
            
            //Check for whiteBall potted. If whiteBall gets potted,
            // set the whiteBall to its last position and the turn goes
            // to the other player
            if tempBall.getName() == "whiteBall"{
                
                print("Registerd potted whiteBall!!")
                
                let lastPosition = tempBall.getLastPosition()
                tempBall.setPosition(position: lastPosition)
                
                tempBall.setBallPotted(isPotted: false)
                tempBall.setPottedHole(newHole: nil)
                
                playerPottedBalls = false
                break
            }
            
            //Store the registerd potted ball in the correct player Array
            if self.player1.typeBall == tempBall.getType(){
                
                self.pottedBallsPlayer1.append(tempBall)
                
                //One less than playable balls (which is 7)
                if self.pottedBallsPlayer1.count == 6{
                    self.lastPotHolePlayer1 = tempBall.getPottedHole()
                }
                
                //Keeps track if a player potted any component player balls
                if self.currentPlayer.typePlayer != self.player1.typePlayer {
                    self.playerPottedOpBalls = true
                }
            }else{
                
                self.pottedBallsPlayer2.append(tempBall)
                
                //One less than playable balls (which is 7)
                if self.pottedBallsPlayer2.count == 6{
                    self.lastPotHolePlayer2 = tempBall.getPottedHole()
                }
                
                //Keeps track if a player potted any component player balls
                if self.currentPlayer.typePlayer != self.player2.typePlayer {
                    self.playerPottedOpBalls = true
                }
            }
        }
        
        //if the player did not potted any balls, the turn switches to the other player.
        if !playerPottedBalls || self.playerPottedOpBalls{
        
            if self.currentPlayer.typePlayer == self.player1.typePlayer {
                
                let playerTurnMessage = LocalizationHelper.getInstance().localizePlayerTurnMessage(playerName: self.player2.name)
                self.sendInfoMessage(with: "infoLabelText", message: playerTurnMessage)
                self.currentPlayer = self.player2
            }else{
                
                let playerTurnMessage = LocalizationHelper.getInstance().localizePlayerTurnMessage(playerName: self.player1.name)
                self.sendInfoMessage(with: "infoLabelText", message: playerTurnMessage)
                self.currentPlayer = self.player1
            }
        }else{
            if self.currentPlayer.typePlayer == self.player1.typePlayer {
                
                let playerTurnMessage = LocalizationHelper.getInstance().localizePlayerTurnMessage(playerName: self.player1.name)
                self.sendInfoMessage(with: "infoLabelText", message: playerTurnMessage)
            }else{
                
                let playerTurnMessage = LocalizationHelper.getInstance().localizePlayerTurnMessage(playerName: self.player2.name)
                self.sendInfoMessage(with: "infoLabelText", message: playerTurnMessage)
            }
        }
        
        //Add potted balls to private pottedBalls list
        self.allPottedBalls = tempResult
        
        //Send a request to show all the info text
        NotificationCenter.default.post(name: Global.LabelNotificName, object: nil, userInfo: ["displayInfoText": true])
    }
    
    /**
     Game rules regarding the eight ball. This ball is special, since it
     requires different rules then the rest of the balls
     - parameters:
     - eightBall: the eightBall the user has played
     */
    private func check8ballRules(eightBall: Ball){
        
        if self.currentPlayer.typePlayer == .player1 {
            
            //Check if 8ball is the last ball to play
            if self.pottedBallsPlayer1.count == 7{
                
                let pottedHole = eightBall.getPottedHole()
                let diseredHole = ConversionHelper.getInstance().getOppositeHole(hole: self.lastPotHolePlayer1)
                
                //End game when player1 potted the eightBall
                if pottedHole != diseredHole{
                    self.endGame(winner: self.player2, loser: self.player1, endCondition: .eightBallFinal)
                }else{
                    self.endGame(winner: self.currentPlayer, loser: self.player2, endCondition: .normal)
                }
            }else{
                print("End Game because 8ball potted too early Player 1!")
                //End game when player1 potted the eightBall too early
                self.endGame(winner: self.player2, loser: self.player1, endCondition: .eightBallEarly)
            }
        }else{
            
            //Check if 8ball is the last ball to play
            if self.pottedBallsPlayer2.count == 7{
                
                let pottedHole = eightBall.getPottedHole()
                let diseredHole = ConversionHelper.getInstance().getOppositeHole(hole: self.lastPotHolePlayer2)
                
                //End game when player2 potted the eightBall
                if pottedHole != diseredHole{
                    self.endGame(winner: self.player1, loser: self.player2, endCondition: .eightBallFinal)
                }
                else{
                    self.endGame(winner: self.currentPlayer, loser: self.player1, endCondition: .normal)
                }
            }else{
                
                print("End Game because 8ball potted too early Player 2!")
                //End game when player2 potted the eightBall too early
                self.endGame(winner: self.player1, loser: self.player2, endCondition: .eightBallEarly)
            }
        }
    }
    
    /**
    Determines the type of Ball the player needs to play with, based on the
     first potted ball.
     - parameters:
        - firstPBall: the first ball registerd as potted
    */
    private func determinePlayerTypes(firstPBall: Ball){
        
        self.currentPlayer.typeBall = firstPBall.getType()
        
        //Determine the other BallType based on the BallType
        // of the current player
        var otherBallType: BallType!
        if self.currentPlayer.typeBall == BallType.HalfBall {
            otherBallType = BallType.FullBall
        }else{
            otherBallType = BallType.HalfBall
        }
        
        //Set BallType for the other player then the current player
        if self.currentPlayer.typePlayer == self.player1.typePlayer{
            self.player2.typeBall = otherBallType
            self.player1.typeBall = self.currentPlayer.typeBall
        }else{
            self.player1.typeBall = otherBallType
            self.player2.typeBall = self.currentPlayer.typeBall
        }
    }
    
    /**
    Handles logic when the game has ended, because a player won,
     or a player made a critical mistake, and therefore lets the other player win
    */
    private func endGame(winner: Player, loser: Player, endCondition: EndGameCondition){
        
        var gameOverMessage: String!
        
        switch endCondition {
        case .normal:
            
            gameOverMessage = LocalizationHelper.getInstance().localizeGameOverMessage(key: "normalGameOver", winner: winner.name, loser: loser.name)
            break
        case .eightBallEarly:
    
            gameOverMessage = LocalizationHelper.getInstance().localizeGameOverMessage(key: "eightEarlyGameOver", winner: winner.name, loser: loser.name)
            break
        case .eightBallFinal:

            gameOverMessage = LocalizationHelper.getInstance().localizeGameOverMessage(key: "eightFinalGameOver", winner: winner.name, loser: loser.name)
            break
        }
        
        //Activate the gameOver screen through GameViewController
        self.delegate?.showGameMenuScreen(withMessage: gameOverMessage)
    }
    
    /**
    Resets the whole game and its properties.
    */
    func resetGame(){
        
        self.player1 = Player(typePlayer: .player1, typeBall: nil, name: self.player1.name)
        self.player2 = Player(typePlayer: .player2, typeBall: nil, name: self.player2.name)
        self.currentPlayer = self.player1
        
        self.allPottedBalls.removeAll()
        self.pottedBallsPlayer1.removeAll()
        self.pottedBallsPlayer2.removeAll()
        
        self.roundPlayed = true
        self.playerPottedOpBalls = false
        self.lastPotHolePlayer1 = nil
        self.lastPotHolePlayer2 = nil
        
        self.whiteBallVelo = 0
        
        Balls.getInstance().resetBallPositions()
        
        //Reset infoLabel of HUD
        self.sendInfoMessage(with: "infoLabelText", message: LocalizationHelper.getInstance().localizePlayerTurnMessage(playerName: Game.getInstance().getPlayer1Name()))
        
        self.sendInfoMessage(with: "veloLabelValue", message: "0")
    }
    
    /**
     Send a message to the Observer which handles the information for the Info Label in the HUD
     - parameters:
     - message: The message to display in the infoLabel of the HUD
     */
    private func sendInfoMessage(with prefix: String, message: String){
        
        NotificationCenter.default.post(name: Global.LabelNotificName, object: nil, userInfo: [prefix: message])
    }
}
