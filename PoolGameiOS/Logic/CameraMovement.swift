//
//  CameraMovement.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 27-07-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import Foundation
import SceneKit

class CameraMovement {
    
    private static let instance = CameraMovement()
    
    private var tempPoint: CGPoint!
    
    private let cameraSpeed: Float = 0.03
    
    public static func getInstance() -> CameraMovement {
        return instance
    }
    
    private init(){}
    
    /**
     Handles the camera positioning and rotation based on the user's touch input
     - parameters:
     - location: the location on screen the user tapped
     */
    func handleCameraRotation(camera: SCNNode, location: CGPoint){
        
        if let temp = self.tempPoint {
            
            var tempEuler = camera.eulerAngles
            
            //Check for new x (horizontal) Euler
            if location.x - temp.x >= 0.1 {
                
                /*if ConversionHelper.getInstance().convertRadToDegree(rad: tempEuler.y) >= 360 {
                    tempEuler.y = 0
                }*/
                
                tempEuler = SCNVector3(x: tempEuler.x, y: tempEuler.y - self.cameraSpeed, z: tempEuler.z)
                camera.eulerAngles = tempEuler
            }
            else if location.x - temp.x < -0.1 {
                
                /*if ConversionHelper.getInstance().convertRadToDegree(rad: tempEuler.y) <= -360 {
                    tempEuler.y = 0
                }*/
                
                tempEuler = SCNVector3(x: tempEuler.x, y: tempEuler.y + self.cameraSpeed, z: tempEuler.z)
                camera.eulerAngles = tempEuler
            }
            
            //Check for y (vertical) Euler
            if location.y - temp.y >= 0.1 {
                if ConversionHelper.getInstance().convertRadToDegree(rad: tempEuler.x) >= -45 {
                    tempEuler = SCNVector3(x: tempEuler.x - self.cameraSpeed, y: tempEuler.y, z: tempEuler.z)
                    camera.eulerAngles = tempEuler
                }
                
            }
            else if location.y - temp.y < -0.1 {
                
                if ConversionHelper.getInstance().convertRadToDegree(rad: tempEuler.x) <= 0 {
                    tempEuler = SCNVector3(x: tempEuler.x + self.cameraSpeed, y: tempEuler.y, z: tempEuler.z)
                    camera.eulerAngles = tempEuler
                }
            }
            
            //Calculate new whiteBall direction, based on changes from the camera
            let whiteBallDirection = Calculations.getInstance().calculateWhiteBallDirection(eulerY: camera.eulerAngles.y)
            
            //Set new direction for the whiteBall, but only after a round has played.
            //During a round you will not change the balls direction
            if Game.getInstance().getRoundPlayed(){
                Game.getInstance().updateDirection(newDir: whiteBallDirection)
            }
        }
        
        self.tempPoint = location
    }
}
