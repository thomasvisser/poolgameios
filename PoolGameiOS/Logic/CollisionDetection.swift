//
//  CollisionDetection.swift
//  PoolGameiOS
//
//  Created by Thomas Visser on 15-07-18.
//  Copyright © 2018 Thomas Visser. All rights reserved.
//

import Foundation
import SceneKit

protocol TableRequestsDelegate {
    
    func requestTableDimensions() -> (width: Float, height: Float)
    func requestTableCollisionWall(cType: CollisionWallType) -> SCNNode
    func requestMidHoles() -> Array<SCNNode>
    func requestCornerHoles() -> Array<SCNNode>
}

/**
Handles all the physics collisions of the game. It checks Wall Collisions
between balls and walls, and it checks Ball Collisions between 2 or more balls.
*/
class CollisionDetection {
    
    private static var instance = CollisionDetection()
    
    public static func getInstance() -> CollisionDetection{
        return instance
    }
    
    static var delegate: TableRequestsDelegate?
    
    private init(){}
    
    /**
    Handles Collision of a Ball with the sides of the Pool Table
    - parameters:
        - ball: the Ball Object to check collision for
    */
    public func checkWallCollision(ball: Ball){
        
        let ballPosX = ball.getPosition().x
        let ballPosZ = ball.getPosition().z
        
        let CtopWall: SCNVector3 = (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Top).position)!
        let CtopWallXMin: Float = CtopWall.x + (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Top).geometry?.boundingBox.min.x)!
        let CtopWallXMax: Float = CtopWall.x + (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Top).geometry?.boundingBox.max.x)!
        
        let CbottomWall: SCNVector3 = (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Bottom).position)!
        let CbottomWallXMin: Float = CbottomWall.x + (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Bottom).geometry?.boundingBox.min.x)!
        let CbottomWallXMax: Float = CbottomWall.x + (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Bottom).geometry?.boundingBox.max.x)!
        
        //Min and Max positioning determination is reversed. This is done, to read the minimum and maximum positions of the wall from left to right
        //(Left edge side of the wall is the Min and Right edge side of the wall is the Max)
        let CleftTopWall: SCNVector3 = (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Left_Top).position)!
        let CleftTopWallZMin: Float = CleftTopWall.z + (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Left_Top).geometry?.boundingBox.max.x)!
        let CleftTopWallZMax: Float = CleftTopWall.z + (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Left_Top).geometry?.boundingBox.min.x)!
        
        //Min and Max positioning determination is reversed, this is done, to read the minimum and maximum positions of the wall from left to right
        //(Left edge side of the wall is the Min and Right edge side of the wall is the Max)
        let CleftBottomWall: SCNVector3 = (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Left_Bottom).position)!
        let CleftBottomWallZMin: Float = CleftBottomWall.z + (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Left_Bottom).geometry?.boundingBox.max.x)!
        let CleftBottomWallZMax: Float = CleftBottomWall.z + (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Left_Bottom).geometry?.boundingBox.min.x)!
        
        let CrightTopWall: SCNVector3 = (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Right_Top).position)!
        let CrightTopWallZMin: Float = CrightTopWall.z + (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Right_Top).geometry?.boundingBox.min.x)!
        let CrightTopWallZMax: Float = CrightTopWall.z + (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Right_Top).geometry?.boundingBox.max.x)!
        
        let CrightBottomWall: SCNVector3 = (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Right_Bottom).position)!
        let CrightBottomWallZMin: Float = CrightBottomWall.z + (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Right_Bottom).geometry?.boundingBox.min.x)!
        let CrightBottomWallZMax: Float = CrightBottomWall.z + (CollisionDetection.delegate?.requestTableCollisionWall(cType: .Wall_Right_Bottom).geometry?.boundingBox.max.x)!
        
        if(Float(ballPosZ + ball.getRadius()) >= CbottomWall.z &&
            ((Float(ballPosX + ball.getRadius()) >= CbottomWallXMin) && (Float(ballPosX - ball.getRadius()) <= CbottomWallXMax))){
            //Collision Bottom side of Table
            //print("Bottom side Collision!")
            
            let tempDirection = ball.getDirection()
            ball.setDirection(newDirection: SCNVector3(x: tempDirection.x, y: tempDirection.y, z: -tempDirection.z))
        }
        else if(Float(ballPosZ - ball.getRadius()) <= CtopWall.z &&
            ((Float(ballPosX + ball.getRadius()) >= CtopWallXMin) && (Float(ballPosX - ball.getRadius()) <= CtopWallXMax))){
            //Collision Top side of Table
            //print("Top side Collision!")
            
            let tempDirection = ball.getDirection()
            ball.setDirection(newDirection: SCNVector3(x: tempDirection.x, y: tempDirection.y, z: -tempDirection.z))
        }
        else if(Float(ballPosX - ball.getRadius()) <= CleftTopWall.x &&
            ((Float(ballPosZ - ball.getRadius()) <= CleftTopWallZMin) && (Float(ballPosZ + ball.getRadius()) >= CleftTopWallZMax))){
            //Collision Left Top side of Table
            //print("Left Topside Collision!")
            
            let tempDirection = ball.getDirection()
            ball.setDirection(newDirection: SCNVector3(x: -tempDirection.x, y: tempDirection.y, z: tempDirection.z))
        }
        else if(Float(ballPosX - ball.getRadius()) <= CleftBottomWall.x &&
            ((Float(ballPosZ - ball.getRadius()) <= CleftBottomWallZMin) && (Float(ballPosZ - ball.getRadius()) >= CleftBottomWallZMax))){
            //Collision Left Bottom side of Table
            //print("Left Bottomside Collision!")
            
            let tempDirection = ball.getDirection()
            ball.setDirection(newDirection: SCNVector3(x: -tempDirection.x, y: tempDirection.y, z: tempDirection.z))
        }
        else if(Float(ballPosX + ball.getRadius()) >= CrightTopWall.x &&
            ((Float(ballPosZ + ball.getRadius()) >= CrightTopWallZMin) && (Float(ballPosZ - ball.getRadius()) <= CrightTopWallZMax))){
            //Collision Right Top side of Table
            //print("Right Topside Collision!")
            
            let tempDirection = ball.getDirection()
            ball.setDirection(newDirection: SCNVector3(x: -tempDirection.x, y: tempDirection.y, z: tempDirection.z))
        }
        else if(Float(ballPosX + ball.getRadius()) >= CrightBottomWall.x &&
            ((Float(ballPosZ + ball.getRadius()) >= CrightBottomWallZMin) && (Float(ballPosZ - ball.getRadius()) <= CrightBottomWallZMax))){
            //Collision Right Bottom side of Table
            //print("Right Bottomside Collision!")
                    
            let tempDirection = ball.getDirection()
            ball.setDirection(newDirection: SCNVector3(x: -tempDirection.x, y: tempDirection.y, z: tempDirection.z))
        }
    }
    
    /**
    Handles Collision of a Ball with the Holes of the Pool Table
    - parameters:
        - ball: the Ball Object to check collision for
    */
    public func checkHolesCollision(ball: Ball){
        
        let midHoles = CollisionDetection.delegate?.requestMidHoles()
        let cornerHoles = CollisionDetection.delegate?.requestCornerHoles()
        
        guard let mHoles = midHoles else { print("No Holes present in midHoles Array @Table!"); return}
        
        //Checks Collision for the Mid-Holes
        for hole in mHoles {
            
            let prefixName = hole.name!
            
            for holeWall in hole.childNodes{
            
                if (holeWall.name! == (prefixName + "_coll_wall_right")) ||
                    (holeWall.name! == (prefixName + "_coll_wall_left")){
                    
                    /*var ballZPosition: Float!
                    let ballXPosition: Float = ball.getPosition().x - ball.getRadius()
                    
                    var returnValues: (difX: Float, difZ: Float, touchesLine: Bool) = Calculations.getInstance().calcIfBallPosTouchesWall(wall: holeWall, pos: (ballXPosition, ballZPosition))
                    
                    //Check for right wall collision of the Hole
                    if(holeWall.name! == (prefixName + "_coll_wall_right")){
                        
                        ballZPosition = ball.getPosition().z - ball.getRadius()
                        
                        if (ballZPosition <= holeWall.position.z) &&
                            (ballXPosition >= holeWallXMin) && (ballXPosition <= holeWallXMin){
                            
                            if ball.getName() == "oneBall"{
                                print("collision hole wall right!")
                            }
                            
                            ball.setDirection(newDirection: SCNVector3(x: 0, y: -1, z: 0))
                        }
                    //Check for left wall collision of the Hole
                    }else if(holeWall.name! == (prefixName + "_coll_wall_left")){
                        
                        ballZPosition = ball.getPosition().z + ball.getRadius()
                        
                        if (ballZPosition >= holeWall.position.z) &&
                            (ballXPosition >= holeWallXMax) && (ballXPosition <= holeWallXMin){
                            
                            if ball.getName() == "oneBall"{
                                print("collision hole wall right!")
                            }
                            
                            ball.setDirection(newDirection: SCNVector3(x: 0, y: -1, z: 0))
                        }
                    }*/
                }
                //Check for back collisision of the Hole
                if (holeWall.name! == (prefixName + "_coll_wall_back")){
                    
                    let holeWallZMin = holeWall.position.z + (holeWall.geometry?.boundingBox.min.x)!
                    let holeWallZMax = holeWall.position.z + (holeWall.geometry?.boundingBox.max.x)!
                    var ballXPosition: Float!
                    //let ballYPosition: Float = ball.getPosition().y - ball.getRadius()
                    let ballZPosition: Float = ball.getPosition().z - ball.getRadius()
                    
                    ballXPosition = ball.getPosition().x - ball.getRadius()
                
                    if (ballXPosition <= holeWall.position.x) &&
                        (ballZPosition >= holeWallZMin) && (ballZPosition <= holeWallZMax) {
                        
                        ball.setDirection(newDirection: SCNVector3(x: 0, y: -1, z: 0))
                    }
                }
                else if holeWall.name! == (prefixName + "_coll_floor"){
                
                    //Check for floor collision of the Hole
                    if (ball.getPosition().y - ball.getRadius()) <= holeWall.position.y {
                        
                        ball.setDirection(newDirection: SCNVector3(x: 0, y: 0, z: 0))
                        ball.setVelocity(newVelocity: 0)
                        
                        //If ball hit the Floor of the Hole, set ball as potted
                        ball.setBallPotted(isPotted: true)
                    }
                }
            }
        }
        
        guard let cHoles = cornerHoles else { print("No Holes present in cornerHoles Array @Table!"); return}

        //Checks Collision for the Corner-Holes
        for hole in cHoles {
            
            let prefixName = hole.name!
            
            for holeWall in hole.childNodes {
                
                if (holeWall.name! == (prefixName + "_coll_wall_right")) ||
                    (holeWall.name! == (prefixName + "_coll_wall_left")){
                    //Add Logic for manual calculations
                }
                //Check for back collisision of the Hole
                if (holeWall.name! == (prefixName + "_coll_wall_back")){
                    //Add Logic for manual calculations
                }
                else if holeWall.name! == (prefixName + "_coll_floor"){
                    
                    //Check for floor collision of the Hole
                    if (ball.getPosition().y - ball.getRadius()) <= holeWall.position.y {
                        
                        ball.setDirection(newDirection: SCNVector3(x: 0, y: 0, z: 0))
                        ball.setVelocity(newVelocity: 0)
                        
                        //If ball hit the Floor of the Hole, set ball as potted
                        ball.setBallPotted(isPotted: true)
                    }
                }
            }
        }
    }
    
    private var tempCheckBallCounter = 0
    
    /**
    Handles Collision of a Ball with another Ball
    */
    public func checkBallCollision(ballA: Ball, ballB: Ball){
        
        //Store radius of the ball temporarily.
        //Since all the balls have the same radius, load one of them (BallA)
        let hitDistance = (ballA.getRadius() * 2)
        
        let positionBallA = ballA.getPosition()
        let positionBallB = ballB.getPosition()
        
        //Calculate the distance between the 2 balls for the x and z values
        let difX = abs(positionBallB.x - positionBallA.x)
        let difZ = abs(positionBallB.z - positionBallA.z)
    
        //Check for collision between BallA and BallB
        if (difX <= hitDistance) && (difZ <= hitDistance) && (ballA.getVelocity() > 0 || ballB.getVelocity() > 0){
            
            //The Unit Normal Vector of the collisiom. Used to calculate path of ball B
            let normalVector = SCNVector3(x: ballB.getPosition().x - ballA.getPosition().x, y: 0, z: ballB.getPosition().z - ballA.getPosition().z).normalized()
            
            //The Unit Tangent Vector of the collision. Used to calculate path of Ball A
            let tangentVector = SCNVector3(x: -normalVector.z, y: 0, z: normalVector.x)
            
            //Put the Ball Velocity into a vector representation of the velocity
            let ballAVelocity = SCNVector3(ballA.getDirection().x * ballA.getVelocity(), 0, ballA.getDirection().z * ballA.getVelocity())
            let ballBVelocity = SCNVector3(ballB.getDirection().x * ballB.getVelocity(), 0, ballB.getDirection().z * ballB.getVelocity())
            
            //Calculate the Normal and Tangent components of the ball speed (Before Collision)
            let ballANormal = normalVector.dot(vector: ballAVelocity)
            let ballBNormal = normalVector.dot(vector: ballBVelocity)
            let ballATangent = tangentVector.dot(vector: ballAVelocity)
            let ballBTangent = tangentVector.dot(vector: ballBVelocity)
            
            //Tangential Component of the Balls after the collision, is the same
            // as before the collision
            let ballATangentAfter = ballATangent
            let ballBTangentAfter = ballBTangent
            
            //Calculate the Normal Component of the Balls after the collision using
            // the One-Dimensional Elastic Collision calculation
            //let ballANormalAfter = (hitDistance * ballBNormal) / hitDistance
            //let ballBNormalAfter = (hitDistance * ballANormal) / hitDistance
            let ballANormalAfter = ((ballANormal * 0) + 2 * 1 * ballBNormal)/2
            let ballBNormalAfter = ((ballBNormal * 0) + 2 * 1 * ballANormal)/2
            
            //Convert the scalar Normal and Tangent components into vectors
            let ballANormalVector = SCNVector3Copy(vector: normalVector) * ballANormalAfter
            let ballBNormalVector = SCNVector3Copy(vector: normalVector) * ballBNormalAfter
            let ballATangentVector = SCNVector3Copy(vector: tangentVector) * ballATangentAfter
            let ballBTangentVector = SCNVector3Copy(vector: tangentVector) * ballBTangentAfter
            
            //The final velocities for the Balls
            let ballAFinalVelocity = SCNVector3Copy(vector: ballANormalVector) + ballATangentVector
            let ballBFinalVelocity = SCNVector3Copy(vector: ballBNormalVector) + ballBTangentVector
            
            //New Ball direction
            let newBallADir = SCNVector3(x: ballAFinalVelocity.x, y: 0, z: ballAFinalVelocity.z)
            let newBallBDir = SCNVector3(x: ballBFinalVelocity.x, y: 0, z: ballBFinalVelocity.z)
            
            //New Ball speed
            let newBallASpeed = ballAFinalVelocity.length()
            let newBallBSpeed = ballBFinalVelocity.length()
            
            //Set new directions and speed
            ballA.setDirection(newDirection: newBallADir.normalized())
            ballB.setDirection(newDirection: newBallBDir.normalized())
            
            ballA.setVelocity(newVelocity: newBallASpeed)
            ballB.setVelocity(newVelocity: newBallBSpeed)
            
            tempCheckBallCounter = tempCheckBallCounter + 1
        
            //DEBUG
            /*print("Collision between Ball: \(ballA.getName()) and Ball: \(ballB.getName())")
            print("Count: \(tempCheckBallCounter)")
            print("Normal vector: \(normalVector)")
            print("Tangent vector: \(tangentVector)")
             
            print("Initial BallAV: \(ballA.getVelocity()) Initial BallBV: \(ballB.getVelocity())")
            print("Initial BallAD: \(ballA.getDirection()) Initial BallBD: \(ballB.getDirection())")
            
            print("ballAVelocity: \(ballAVelocity)")
            print("ballBVelocity: \(ballBVelocity)")
            
            print("ballANormalVector: \(ballANormal)")
            print("ballBNormalVector: \(ballBNormal)")
            print("ballATangentVector: \(ballATangent)")
            print("ballBTangentVector: \(ballBTangent)")
            
            print("ballANormalAfter: \(ballANormalAfter)")
            print("ballBNormalAfter: \(ballBNormalAfter)")
            
            print("BallANormalVector \(ballANormalVector)")
            print("BallBNormalVector \(ballBNormalVector)")
            print("BallATangentVector \(ballATangentVector)")
            print("BallBTangentVector \(ballBTangentVector)")
            
            print("BallA FinalVelo: \(ballAFinalVelocity)")
            print("BallB FinalVelo: \(ballBFinalVelocity)")
            
            print("New Ball A Dir: \(newBallADir)")
            print("New Ball B Dir: \(newBallBDir)")
            print("New Ball A Dir Norm: \(newBallADir.normalized())")
            print("New Ball B Dir Norm: \(newBallBDir.normalized())")
            print("New Ball A Velo: \(newBallASpeed)")
            print("New Ball B Velo: \(newBallBSpeed)\n")*/
        }
    }
}
